//
//  BSRoundButton.h
//  BloomService
//
//  Created by user on 23.03.16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSRoundButton : UIButton

- (void)setupStyle;

@end
