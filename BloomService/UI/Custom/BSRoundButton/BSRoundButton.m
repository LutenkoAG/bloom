//
//  BSRoundButton.m
//  BloomService
//
//  Created by user on 23.03.16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import "BSRoundButton.h"
#import "QuartzCore/QuartzCore.h"

@implementation BSRoundButton

- (void)setupStyle {
    CGFloat cornerRadius = self.frame.size.height / 2.0f;
//    [[self layer] setBorderWidth:2.0f];
    [self.layer setMasksToBounds:YES];
    self.layer.cornerRadius = cornerRadius;
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    [self setupStyle];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
