//
//  BSSegmentedControl.m
//  BloomService
//
//  Created by Ruslan Shevtsov on 7/5/16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import "BSSegmentedControl.h"

@implementation BSSegmentedControl

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    NSInteger current = self.selectedSegmentIndex;
    [super touchesBegan:touches withEvent:event];
    if (current == self.selectedSegmentIndex)
        [self sendActionsForControlEvents:UIControlEventValueChanged];
}

@end
