//
//  BSSegmentedControl.h
//  BloomService
//
//  Created by Ruslan Shevtsov on 7/5/16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSSegmentedControl : UISegmentedControl

@end
