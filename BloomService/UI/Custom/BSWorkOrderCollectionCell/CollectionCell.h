//
//  CollectionCell.h
//  BloomService
//
//  Created by user on 28.03.16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PWProgressView.h"

@class CollectionCell;

@protocol CollectionCellDelegate <NSObject>

- (void)collectionCellDeleteImage:(CollectionCell *)cell;

@end


@interface CollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *numberOfPicture;
@property (strong, nonatomic) PWProgressView *progressView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicatorView;

@property (weak, nonatomic) id <CollectionCellDelegate> delegate;

@end
