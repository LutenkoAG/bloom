//
//  CollectionCell.m
//  BloomService
//
//  Created by user on 28.03.16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import "CollectionCell.h"

@implementation CollectionCell

- (IBAction)deleteImageAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(collectionCellDeleteImage:)]) {
        [self.delegate collectionCellDeleteImage:self];
    }
}

@end
