//
//  BSEdetiWorkOrderEquipmentCell.m
//  BloomService
//
//  Created by Aleksandr Lutenko on 10.06.16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import "BSEdetiWorkOrderEquipmentCell.h"



@implementation BSEdetiWorkOrderEquipmentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateFormat:@"YYYY-MM-dd"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)deleteButton:(UIButton *)sender {
    
    if ([self.delegate respondsToSelector:@selector(editWorkOrderEquipmentCellDeleteAction:)]) {
        [self.delegate editWorkOrderEquipmentCellDeleteAction:self];
    }
}

-(void)setupCellWithEquipment: (BSPartsOrLabor *) equipment {
    self.nameLabel.text = equipment.name;
    self.quantityLabel.text = [NSString stringWithFormat:@"%@", equipment.count];
    self.typeLabel.text = equipment.type;
    CGFloat ftrate = [equipment.rate floatValue];
    self.rateLabel.text = [NSString stringWithFormat:@"%.2f", ftrate];
    self.amountLabel.text = [NSString stringWithFormat:@"%.2f", [equipment.count floatValue] * [equipment.rate floatValue]];
    self.dateLabel.text = [self.dateFormatter stringFromDate:equipment.date];
    CGFloat fCostQty = [equipment.costQty floatValue];
    self.costQuantityLabel.text = [NSString stringWithFormat:@"%.2f", fCostQty];
}

@end
