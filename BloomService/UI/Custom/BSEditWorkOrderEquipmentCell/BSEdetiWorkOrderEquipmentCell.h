//
//  BSEdetiWorkOrderEquipmentCell.h
//  BloomService
//
//  Created by Aleksandr Lutenko on 10.06.16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSPartsOrLabor.h"

@class BSEdetiWorkOrderEquipmentCell;

@protocol BSEdetiWorkOrderEquipmentCellDelegate <NSObject>

@required

- (void)editWorkOrderEquipmentCellDeleteAction:(BSEdetiWorkOrderEquipmentCell *)cell;

@end

@interface BSEdetiWorkOrderEquipmentCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UILabel *quantityLabel;
@property (weak, nonatomic) IBOutlet UILabel *rateLabel;
@property (weak, nonatomic) IBOutlet UILabel *amountLabel;
@property (weak, nonatomic) IBOutlet UILabel *costQuantityLabel;
@property (strong, nonatomic) NSDateFormatter *dateFormatter;

@property (nonatomic, assign) id<BSEdetiWorkOrderEquipmentCellDelegate> delegate;

-(void)setupCellWithEquipment: (BSPartsOrLabor *) equipment;
@end
