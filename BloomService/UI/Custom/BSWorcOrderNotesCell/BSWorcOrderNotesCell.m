//
//  BSWorcOrderNotesCell.m
//  BloomService
//
//  Created by Aleksandr Lutenko on 26.08.16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import "BSWorcOrderNotesCell.h"

@implementation BSWorcOrderNotesCell

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

- (IBAction)deleteCommentAction:(id)sender {
    
    if ([self.delegate respondsToSelector:@selector(editWorkOrderCommentCellDeleteAction:)]) {
        [self.delegate editWorkOrderCommentCellDeleteAction:self];
    }
}

@end
