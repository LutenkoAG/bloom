//
//  BSWorcOrderNotesCell.h
//  BloomService
//
//  Created by Aleksandr Lutenko on 26.08.16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BSWorcOrderNotesCell;

@protocol BSEdetiWorkOrderCommentCellDelegate <NSObject>

@required

- (void)editWorkOrderCommentCellDeleteAction:(BSWorcOrderNotesCell *)cell;

@end

@interface BSWorcOrderNotesCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *notesLabel;

@property (nonatomic, assign) id<BSEdetiWorkOrderCommentCellDelegate> delegate;

@end
