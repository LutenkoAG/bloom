//
//  UIColor+CustomColors.h
//  BloomService
//
//  Created by Oleg Vishnivetskiy on 12/08/16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (CustomColors)

+ (UIColor *)r:(CGFloat)red g:(CGFloat)green b:(CGFloat)blue;

+ (UIColor *)standartRedColor;

+ (UIColor *)standartGrayColor;

+ (UIColor *)grayColorWithValue:(CGFloat)value;

@end
