//
//  UIColor+CustomColors.m
//  BloomService
//
//  Created by Oleg Vishnivetskiy on 12/08/16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import "UIColor+CustomColors.h"

@implementation UIColor (CustomColors)

+ (UIColor *)r:(CGFloat)red g:(CGFloat)green b:(CGFloat)blue {
    return [UIColor colorWithRed:red/255.0f green:green/255.0f blue:blue/255.0f alpha:1.0];
}

+ (UIColor *)standartRedColor {
    return [UIColor r:208 g:36 b:42];
}

+ (UIColor *)standartGrayColor {
    return [UIColor grayColorWithValue:237];
}

+ (UIColor *)grayColorWithValue:(CGFloat)value {
    return [UIColor r:value g:value b:value];
}


@end
