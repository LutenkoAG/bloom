//
//  NSDictionary+Extended.m


#import "NSDictionary+Extended.h"

@implementation NSDictionary (Extended)

- (id)tryObjectForKey:(id)key
{
    id object = [self objectForKey:key];
    
    if (object == nil || [object isKindOfClass:[NSNull class]])
    {
        return nil;
    }
    
    return object;
}


@end

@implementation NSMutableDictionary (Extended)

- (BOOL)trySetObject:(id)object forKey:(id)key
{
    if (object)
    {
        [self setObject:object forKey:key];
        return YES;
    }
    else
    {
        return NO;
    }
}

@end
