//
//  NSDictionary+Extended.h


#import <Foundation/Foundation.h>

@interface NSDictionary (Extended)

- (id)tryObjectForKey:(id)key;

@end

@interface NSMutableDictionary (Extended)

- (BOOL)trySetObject:(id)object forKey:(id)key;

@end
