//
//  BSBorderLabel.m
//  BloomService
//
//  Created by user on 25.03.16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import "BSBorderLabel.h"

@implementation BSBorderLabel

- (void)setupStyle {
    [[self layer] setBorderWidth:2.0f];
    [self.layer setMasksToBounds:YES];
    
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    [self setupStyle];
}


@end
