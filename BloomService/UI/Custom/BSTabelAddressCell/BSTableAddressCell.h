//
//  BSTableAddressCell.h
//  BloomService
//
//  Created by user on 25.03.16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSTableAddressCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UILabel *customerLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *numberWorkorder;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;

@end
