//
//  BSAddOneEqyupment.m
//  BloomService
//
//  Created by Aleksandr Lutenko on 14.06.16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import "BSAddOneEqyupment.h"

// BS
#import "BSBusinessFacade.h"
#import "BSDataManager.h"

// UI
#import "UIViewController+HUD.h"
#import "BSEquipmentDescriptionsViewController.h"
#import "UIViewController+Alert.h"
#import "UIColor+CustomColors.h"

// Entities
#import "BSWorkOrder.h"

static NSString * const kShowEquipmentDescriptionsScreenIndetefier = @"ShowEquipmentDescriptionsScreen";

@interface BSAddOneEqyupment () <BSEquipmentDescriptionsViewControllerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *saveButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UITextField *quantityTextField;
@property (weak, nonatomic) IBOutlet UILabel *selectRateLabel;
@property (weak, nonatomic) IBOutlet UILabel *selectDateLabel;
@property (weak, nonatomic) IBOutlet UILabel *selectAmountLabel;
@property (weak, nonatomic) IBOutlet UIButton *chooseEquipmentButton;
@property (weak, nonatomic) IBOutlet UILabel *chooseEquipmentLabel;
@property (weak, nonatomic) IBOutlet UIView *datePikerView;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePiker;
@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (weak, nonatomic) IBOutlet UIView *costQuantityView;
@property (weak, nonatomic) IBOutlet UIButton *laborButton;
@property (weak, nonatomic) IBOutlet UIButton *partsButton;
@property (weak, nonatomic) IBOutlet UILabel *costQuantityLabel;

@property (strong, nonatomic) NSDate *date;
@property (strong, nonatomic) NSString *equipmentType;
@property (strong, nonatomic) NSNumber *part;
@property (strong, nonatomic) NSNumber *repair;
@property (strong, nonatomic) NSString *jCCostCode;
@property (strong, nonatomic) NSString *objectId;
@property (strong, nonatomic) NSString *inactive;
@property (strong, nonatomic) NSString *emploee;


@end

@implementation BSAddOneEqyupment

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    
    [[self.laborButton layer] setBorderWidth:1.0f];
    [[self.partsButton layer] setBorderWidth:1.0f];
    
    [[self.saveButton layer] setBorderWidth:1.0f];
    [[self.cancelButton layer] setBorderWidth:1.0f];
    
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateFormat:@"MM/dd/YYYY"];
    
    self.chooseEquipmentLabel.text = self.editEquipment.name;
    
    if (self.mode == EquipmentModeEdit) {
        
        if ([self.editEquipment.type isEqualToString:kLabor]){
            NSArray *laborDescriptions = [BSBusinessFacade dataManager].repairDescriptions;
            for (BSRepairDescription *labordescription in laborDescriptions) {
                if ([labordescription.text isEqualToString:self.editEquipment.name]) {
                    self.repair = labordescription.repair;
                    self.jCCostCode = labordescription.jCCostCode;
                    self.objectId = labordescription.objectId;
                    self.inactive = labordescription.inactive;
                }
            }
            self.equipmentType = kLabor;
            [self.laborButton setBackgroundColor:[UIColor standartRedColor]];
            [self.laborButton setTintColor:[UIColor whiteColor]];
            [self.partsButton setTintColor:[UIColor standartRedColor]];
            [self.partsButton setBackgroundColor:[UIColor standartGrayColor]];
        } else {
            NSArray *partsDescriptions = [BSBusinessFacade dataManager].partDescriptions;
            for (BSPartDescription *partdescription in partsDescriptions){
                NSString *str = (NSString *)[self.editEquipment.name substringFromIndex:8];
                if ([partdescription.text isEqualToString: str]) {
                    self.part = partdescription.part;
                }
            }
            self.equipmentType = kParts;
            [self.partsButton setBackgroundColor:[UIColor standartRedColor]];
            [self.partsButton setTintColor:[UIColor whiteColor]];
            [self.laborButton setTintColor:[UIColor standartRedColor]];
            [self.laborButton setBackgroundColor:[UIColor standartGrayColor]];
        }
        self.costQuantityLabel.text = [NSString stringWithFormat:@"%@", self.editEquipment.costQty];
        self.partsButton.enabled = NO;
        self.laborButton.enabled = NO;
        self.selectDateLabel.text = [self.dateFormatter stringFromDate:self.editEquipment.date];
        self.date = self.editEquipment.date;
        self.quantityTextField.text = [NSString stringWithFormat:@"%@", self.editEquipment.count];
        self.selectAmountLabel.text = [NSString stringWithFormat:@"%.2f", [self.editEquipment.count floatValue] * [self.editEquipment.rate floatValue]];
        self.emploee = self.editEquipment.employee;
    } else {
        self.editEquipment = [BSPartsOrLabor new];
        self.editEquipment.equipmentId = @"";
        self.editEquipment.workorderId = [BSBusinessFacade getCachedWorkOrder].workorder;
        self.date = [NSDate date];
        [self.laborButton setBackgroundColor:[UIColor standartRedColor]];
        [self.laborButton setTintColor:[UIColor whiteColor]];
        [self.partsButton setTintColor:[UIColor standartRedColor]];
        [self.partsButton setBackgroundColor:[UIColor standartGrayColor]];
        self.equipmentType = kLabor;
        self.quantityTextField.enabled = NO;
    }
    CGFloat ftrate = [self.editEquipment.rate floatValue];
    self.selectRateLabel.text = [NSString stringWithFormat:@"%.2f", ftrate];
    CGFloat ftamount = [self.editEquipment.amount floatValue];
    self.selectAmountLabel.text = [NSString stringWithFormat:@"%.2f", ftamount];

    [self.datePiker setDate:self.date];
    self.selectDateLabel.text = [self.dateFormatter stringFromDate:self.date];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
 
    [self.quantityTextField becomeFirstResponder];
    [self dismissKeyboard];
    return YES;
}

- (IBAction)selectLaborAction:(id)sender {
    [self.laborButton setBackgroundColor:[UIColor standartRedColor]];
    [self.laborButton setTintColor:[UIColor whiteColor]];
    [self.partsButton setTintColor:[UIColor standartRedColor]];
    [self.partsButton setBackgroundColor:[UIColor standartGrayColor]];
    self.equipmentType = kLabor;
    self.quantityTextField.text = @"";
    [self performSegueWithIdentifier:kShowEquipmentDescriptionsScreenIndetefier sender:self];
}

- (IBAction)selectPartsAction:(id)sender {
    [self.partsButton setBackgroundColor:[UIColor standartRedColor]];
    [self.partsButton setTintColor:[UIColor whiteColor]];
    [self.laborButton setTintColor:[UIColor standartRedColor]];
    [self.laborButton setBackgroundColor:[UIColor standartGrayColor]];
    self.equipmentType = kParts;
    self.quantityTextField.text = @"";
    self.costQuantityLabel.text = @"";
    [self performSegueWithIdentifier:kShowEquipmentDescriptionsScreenIndetefier sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:kShowEquipmentDescriptionsScreenIndetefier]) {
        BSEquipmentDescriptionsViewController *vc = segue.destinationViewController;
        if ([self.equipmentType isEqualToString:kLabor]) {
            vc.descriptions = [BSBusinessFacade dataManager].repairDescriptions;
            vc.title = kLabor;
        } else if ([self.equipmentType isEqualToString:kParts]) {
            vc.descriptions = [BSBusinessFacade dataManager].partDescriptions;
            vc.title = kParts;
        }
        vc.delegate = self;
    }
}

- (IBAction)cancelButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)saveButton:(id)sender {
    
    if (self.chooseEquipmentLabel.text == nil
        || self.quantityTextField.text == nil
        || self.selectRateLabel.text == nil
        || self.selectDateLabel.text == nil
        || self.date == nil) {
        [self showAlertOkWithMessage:@"Please, input info!"];
        
        return;
    }
    
    if (self.equipmentType == kParts){
        self.editEquipment.costQty = @([self.costQuantityLabel.text floatValue]);
        self.editEquipment.name = self.chooseEquipmentLabel.text;
        self.editEquipment.count = @([self.quantityTextField.text integerValue]);
        self.editEquipment.rate = [NSNumber numberWithFloat:[self.selectRateLabel.text floatValue]];
        self.editEquipment.type = self.equipmentType;
        self.editEquipment.date = self.date;
        self.editEquipment.part = self.part;
    } else {
        self.editEquipment.costQty = @([self.costQuantityLabel.text floatValue]);
        self.editEquipment.name = self.chooseEquipmentLabel.text;
        self.editEquipment.count = @([self.quantityTextField.text intValue]);
        self.editEquipment.rate = [NSNumber numberWithFloat:[self.selectRateLabel.text floatValue]];
            BSRepairDescription *repairDescription = [BSRepairDescription new];
            repairDescription.jCCostCode = self.jCCostCode;
            repairDescription.repair = self.repair;
            repairDescription.text = self.chooseEquipmentLabel.text;
            repairDescription.objectId = self.objectId;
            repairDescription.inactive = self.inactive;
        self.editEquipment.repairDescription = repairDescription;
        self.editEquipment.type = self.equipmentType;
        self.editEquipment.date = self.date;
    }
    
    BOOL isNewEquipment = NO;
    if (self.mode == EquipmentModeAdd){
        isNewEquipment = YES;
    }
    [self showHUD];
    [BSBusinessFacade editEquipment:self.editEquipment
                       newEquipment:(BOOL)isNewEquipment
                         completion:^(BOOL isSuccess, NSError *error) {
                             [self hideHUD];
                             if (error) {
                                 [self showAlertError:error];
                             } else {
                                 if (isSuccess) {
                                     [BSBusinessFacade updateWorkorders];
                                     [self.navigationController popViewControllerAnimated:YES];
                                 }
                             }
                         }];
    
}

- (IBAction)selectDateButton:(id)sender {
    self.datePikerView.hidden = NO;
}

- (IBAction)datePikerViewOkButtonAction:(id)sender {

    self.date = self.datePiker.date;
    NSDate *selectedDate = self.datePiker.date;
    NSString *stringFromDate = [self.dateFormatter stringFromDate:selectedDate];
    self.selectDateLabel.text = stringFromDate;
    self.datePikerView.hidden = YES;
}

- (IBAction)datePikerViewCancelAction:(id)sender {
    self.datePikerView.hidden = YES;
}

#pragma mark -
#pragma mark BSEquipmentDescriptionsViewControllerDelegate

- (void)selectedDescription:(BSDescroption *)description{
    if (self.equipmentType == kParts){
        self.part = description.part;
        self.chooseEquipmentLabel.text = description.text;
        self.selectRateLabel.text = [NSString stringWithFormat:@"%.2f", [description.partsRate floatValue]];
        self.selectAmountLabel.text = [NSString stringWithFormat:@"%.2f", [self.quantityTextField.text floatValue] * [description.partsRate floatValue]];
    } else {
        self.chooseEquipmentLabel.text = description.text;
        self.selectRateLabel.text = [NSString stringWithFormat:@"%.2f", [description.laborRate floatValue]];
        self.selectAmountLabel.text = [NSString stringWithFormat:@"%.2f", [self.quantityTextField.text floatValue] * [description.laborRate floatValue]];
        self.repair = description.repair;
        self.jCCostCode = description.jCCostCode;
        self.objectId = description.objectId;
        self.inactive = description.inactive;
    }
    self.quantityTextField.enabled = YES;
}

#pragma mark -
#pragma mark UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *someRegexp = @"[0-9]";
    NSPredicate *myTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", someRegexp];
    
    if ([myTest evaluateWithObject:string] || [string isEqualToString:@""]){
        return YES;
    }
    
    return NO;
}

-(void)dismissKeyboard {
    [_quantityTextField resignFirstResponder];
    if (self.equipmentType == kParts){
        self.costQuantityLabel.text = @"";
    } else {
        self.costQuantityLabel.text = self.quantityTextField.text;
    }
    self.selectAmountLabel.text = [NSString stringWithFormat:@"%.2f", [self.quantityTextField.text floatValue] * [self.selectRateLabel.text floatValue]];
}

@end
