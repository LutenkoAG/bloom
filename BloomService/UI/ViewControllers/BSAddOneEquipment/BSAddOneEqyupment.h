//
//  BSAddOneEqyupment.h
//  BloomService
//
//  Created by Aleksandr Lutenko on 14.06.16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSPartsOrLabor.h"

typedef NS_ENUM(NSInteger, EquipmentMode) {
    EquipmentModeEdit,
    EquipmentModeAdd
};

@interface BSAddOneEqyupment : UIViewController

@property (nonatomic, strong) BSPartsOrLabor *editEquipment;
@property (nonatomic, assign) EquipmentMode mode;

@end
