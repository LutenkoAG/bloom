//
//  UIViewController+Alert.m
//  LiftAngel
//
//  Created by Ruslan Shevtsov on 2/12/16.
//  Copyright © 2016 Ruslan Shevtsov. All rights reserved.
//

#import "UIViewController+Alert.h"

@implementation UIViewController (Alert)

- (void)showAlertOkWithTitie:(NSString *)aTitleString message:(NSString *)aMessageText completion:(DefaultAlertBlock)completion {
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:aTitleString
                                          message:aMessageText
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction *okAction = [UIAlertAction actionWithTitle:NSLocalizedString(@"OK", @"OK action")
                                                       style:UIAlertActionStyleDefault
                                                     handler:^(UIAlertAction *action) {
                                                         if (completion) {
                                                             completion();
                                                         }
                                                     }];
    
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
}

- (void)showAlertOkWithTitie:(NSString *)aTitleString message:(NSString *)aMessageText {
    [self showAlertOkWithTitie:aTitleString message:aMessageText completion:nil];
}

- (void)showAlertOkWithMessage:(NSString *)aMessageText {
    [self showAlertOkWithTitie:nil message:aMessageText];
}

- (void)showAlertError:(NSError *)anError {
    [self showAlertOkWithTitie:NSLocalizedString(@"Error", @"Error title") message:anError.localizedDescription];
}

@end
