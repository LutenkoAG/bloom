//
//  UIViewController+Alert.h
//  LiftAngel
//
//  Created by Ruslan Shevtsov on 2/12/16.
//  Copyright © 2016 Ruslan Shevtsov. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^DefaultAlertBlock)();

@interface UIViewController (Alert)

- (void)showAlertError:(NSError *)anError;
- (void)showAlertOkWithMessage:(NSString *)aMessageText;
- (void)showAlertOkWithTitie:(NSString *)aTitleString message:(NSString *)aMessageText;
- (void)showAlertOkWithTitie:(NSString *)aTitleString message:(NSString *)aMessageText completion:(DefaultAlertBlock)completion;

@end
