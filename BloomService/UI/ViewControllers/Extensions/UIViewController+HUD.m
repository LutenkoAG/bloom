//
//  UIViewController+HUD.m
//  LiftAngel
//
//  Created by Ruslan Shevtsov on 3/15/16.
//  Copyright © 2016 Ruslan Shevtsov. All rights reserved.
//

#import "UIViewController+HUD.h"
#import "MBProgressHUD.h"

@implementation UIViewController (HUD)

- (void)showHUD {
    UIView *view = self.view;
    if (self.navigationController) {
        view = self.navigationController.view;
    }
    [MBProgressHUD showHUDAddedTo:view animated:YES];
}

- (void)hideHUD {
    UIView *view = self.view;
    if (self.navigationController) {
        view = self.navigationController.view;
    }
    [MBProgressHUD hideHUDForView:view animated:YES];
}

@end
