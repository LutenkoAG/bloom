//
//  UIViewController+ImagePiker.h
//  LiftAngel
//
//  Created by Ruslan Shevtsov on 2/12/16.
//  Copyright © 2016 Ruslan Shevtsov. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^ImagePikerCompletionBlock)(BOOL isDone, UIImage *image, NSError *anError);

@interface UIViewController (ImagePiker)

- (void)showImagePiker:(ImagePikerCompletionBlock)completion;

@end