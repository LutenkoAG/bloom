//
//  UIViewController+ImagePiker.m
//  LiftAngel
//
//  Created by Ruslan Shevtsov on 2/12/16.
//  Copyright © 2016 Ruslan Shevtsov. All rights reserved.
//

#import "UIViewController+ImagePiker.h"
#import "UIAlertController+Blocks.h"
#import "UIImagePickerController+DelegateBlocks.h"
#import <MobileCoreServices/MobileCoreServices.h>

@implementation UIViewController (ImagePiker)

- (void)showImagePiker:(ImagePikerCompletionBlock)completion {
    static NSString * const kGalleryTitleString = @"Gallery";
    static NSString * const kCameraTitleString = @"Camera";
    
    __weak __typeof(self)weakSelf = self;
    
    UIAlertControllerCompletionBlock tapBlock = ^(UIAlertController *controller,
                                                  UIAlertAction *action,
                                                  NSInteger buttonIndex) {
        if ([action.title isEqualToString:kGalleryTitleString]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf showImagePickerWithSourceType:UIImagePickerControllerSourceTypePhotoLibrary completion:completion];
            });
        } else if ([action.title isEqualToString:kCameraTitleString]) {
            dispatch_async(dispatch_get_main_queue(), ^{
                [weakSelf showImagePickerWithSourceType:UIImagePickerControllerSourceTypeCamera completion:completion];
            });
        }
    };
    
    if ( UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad ) {
        [UIAlertController showAlertInViewController:self
                                           withTitle:@"Choose image"
                                             message:nil
                                   cancelButtonTitle:@"Cancel"
                              destructiveButtonTitle:nil
                                   otherButtonTitles:@[kGalleryTitleString, kCameraTitleString]
                                            tapBlock:tapBlock];
    } else {
        [UIAlertController showActionSheetInViewController:self
                                                 withTitle:@"Choose image"
                                                   message:nil
                                         cancelButtonTitle:@"Cancel"
                                    destructiveButtonTitle:nil
                                         otherButtonTitles:@[kGalleryTitleString, kCameraTitleString]
                        popoverPresentationControllerBlock:nil
                                                  tapBlock:tapBlock];

    }
}

#pragma mark -
#pragma mark UIImagePicker

- (void)showImagePickerWithSourceType:(UIImagePickerControllerSourceType)sourceType
                           completion:(ImagePikerCompletionBlock)completion {
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.allowsEditing = (sourceType == UIImagePickerControllerSourceTypeCamera);
    picker.sourceType = sourceType;
    picker.mediaTypes = @[(NSString *)kUTTypeImage];
    
    [picker useBlocksForDelegate];
    
    [picker onDidFinishPickingMediaWithInfo:^(UIImagePickerController *picker, NSDictionary *info) {
        UIImage *image = nil;
        if (sourceType == UIImagePickerControllerSourceTypeCamera) {
            image = info[UIImagePickerControllerEditedImage];
        } else {
            image = info[UIImagePickerControllerOriginalImage];
        }
        
        [picker dismissViewControllerAnimated:YES completion:nil];

        if (completion) {
            BOOL isDone = (image != nil);
            completion(isDone, image, nil);
        }
        
    }];
    
    [self presentViewController:picker animated:YES completion:nil];
}

@end
