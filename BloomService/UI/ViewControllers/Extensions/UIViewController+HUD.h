//
//  UIViewController+HUD.h
//  LiftAngel
//
//  Created by Ruslan Shevtsov on 3/15/16.
//  Copyright © 2016 Ruslan Shevtsov. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (HUD)

- (void)showHUD;

- (void)hideHUD;

@end
