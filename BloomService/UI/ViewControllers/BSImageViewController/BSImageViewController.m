//
//  BSImageViewController.m
//  BloomService
//
//  Created by Aleksandr Lutenko on 10.06.16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import "BSImageViewController.h"
#import <AFNetworking/UIImageView+AFNetworking.h>
#import "BSBusinessFacade.h"
#import "UIViewController+HUD.h"
#import "MapKit/MapKit.h"
#import "BSAnnotation.h"
#import <CoreLocation/CoreLocation.h>
#import "UIViewController+Alert.h"
#import <PWProgressView/PWProgressView.h>
#import "BSMapChangePinViewController.h"


@interface BSImageViewController () <UITextFieldDelegate>
@property (weak, nonatomic) IBOutlet UITextField *commentTextField;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIView *editCommentView;
@property (weak, nonatomic) IBOutlet UIView *addCommentView;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

@implementation BSImageViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    
    self.descriptionLabel.text = self.picture.descript;
    
    if (self.picture.descript == nil){
        self.addCommentView.hidden = NO;
    }
    self.commentTextField.delegate = self;
    self.addCommentView.hidden = YES;
    
    if (self.picture.image) {
        self.imageView.image = self.picture.image;
    } else {
        [self.imageView setImageWithURL:[NSURL URLWithString:self.picture.bigImageUrl] placeholderImage:[UIImage imageNamed:@"plaseholderImage"]];
    }
    
    self.mapView.mapType = MKMapTypeSatellite;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self reloadMapAnnotations];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (self.picture.isUploading) {
//        [self addProgressView];
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"ChangePin"]) {
        BSMapChangePinViewController *vc = segue.destinationViewController;
        vc.changePin = self.picture;;
    }
}

- (void)addProgressView {
    PWProgressView *progressView = [[PWProgressView alloc] initWithFrame:self.imageView.bounds];
    progressView.progress = self.picture.progress;
    [self.imageView addSubview:progressView];
}

- (void)reloadMapAnnotations {
    [self.mapView removeAnnotations:self.mapView .annotations];
        BSAnnotation *pin = [[BSAnnotation alloc] init];
        pin.coordinate = CLLocationCoordinate2DMake([self.picture.latitude doubleValue], [self.picture.longitude doubleValue]);
        [self.mapView addAnnotation:pin];
        MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:MKCoordinateRegionMakeWithDistance(pin.coordinate, 100, 100)];
        [self.mapView setRegion:adjustedRegion animated:YES];
}

- (IBAction)backButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)saveButton:(id)sender {
    
    if (self.picture.isUploading) {
        [self showAlertOkWithMessage:@"Please wait until the image loaded."];
    } else {
        NSNumber *idImage = self.picture.idImage;
        NSString *description = self.commentTextField.text;
        
        [self showHUD];
        [BSBusinessFacade addNoteImage:idImage description:description completion:^(BOOL isSuccess, NSError *error) {
            [self hideHUD];
            
            if (isSuccess) {
                self.picture.descript = description;
                self.descriptionLabel.text = description;
            } else {
                [self showAlertError:error];
            }
        }];
        
        self.addCommentView.hidden = YES;
    }
}

- (IBAction)editCommentButton:(id)sender {
    
    self.addCommentView.hidden = NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [self.commentTextField becomeFirstResponder];
    [self dismissKeyboard];
    return YES;
}

-(void)dismissKeyboard {
    [_commentTextField resignFirstResponder];
}
@end
