//
//  BSImageViewController.h
//  BloomService
//
//  Created by Aleksandr Lutenko on 10.06.16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSImage.h"


@interface BSImageViewController : UIViewController

@property (nonatomic, strong) BSImage *picture;


@end
