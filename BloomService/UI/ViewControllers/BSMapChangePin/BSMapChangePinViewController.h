//
//  BSMapChangePinViewController.h
//  BloomService
//
//  Created by User on 22/09/16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSAnnotation.h"
#import "BSImage.h"

@interface BSMapChangePinViewController : UIViewController

@property (strong, nonatomic) BSImage *changePin;

@end
