//
//  BSMapChangePinViewController.m
//  BloomService
//
//  Created by User on 22/09/16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import "BSMapChangePinViewController.h"
#import "MapKit/MapKit.h"
#import <CoreLocation/CoreLocation.h>
#import "BSBusinessFacade.h"
#import "BSDataManager.h"
#import "UIViewController+Alert.h"
#import "UIViewController+HUD.h"
#import "BSImageViewController.h"


@interface BSMapChangePinViewController ()

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@property (strong, nonatomic) NSNumber *lat;
@property (strong, nonatomic) NSNumber *lon;

@end

@implementation BSMapChangePinViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.mapView.mapType = MKMapTypeSatellite;
}
-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    UILongPressGestureRecognizer *lpgr = [[UILongPressGestureRecognizer alloc]
                                          initWithTarget:self action:@selector(handleLongPress:)];
    
    lpgr.minimumPressDuration = 0.1; //user needs to press for 0.1 seconds
   
    [self.mapView addGestureRecognizer:lpgr];
    
    [self reloadMapAnnotations];

}

- (void)reloadMapAnnotations {
    [self.mapView removeAnnotations:self.mapView .annotations];
    BSAnnotation *pin = [[BSAnnotation alloc] init];
    pin.coordinate = CLLocationCoordinate2DMake([self.changePin.latitude doubleValue], [self.changePin.longitude doubleValue]);
    [self.mapView addAnnotation:pin];

    MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:MKCoordinateRegionMakeWithDistance(pin.coordinate, 100, 100)];
    [self.mapView setRegion:adjustedRegion animated:YES];
}

- (IBAction)backAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)saveAction:(id)sender {
//    [self showAlertOkWithMessage:@"Pin save"];
    [self showHUD];
    [BSBusinessFacade сhangeImageLocation:self.changePin.idImage latitude:self.changePin.latitude longitude:self.changePin.longitude completion:^(BOOL isSuccess, NSError *error){
        if (isSuccess) {
//            BSImageViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ImegeScreen"];
//            vc.picture = self.changePin;
            [BSBusinessFacade updateWorkorders];
            [self hideHUD];
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            [self showAlertError:error];
        }
    }];
}

- (void)handleLongPress:(UIGestureRecognizer *)gestureRecognizer
{
    if (gestureRecognizer.state != UIGestureRecognizerStateBegan)
        return;
    
    CGPoint touchPoint = [gestureRecognizer locationInView:self.mapView];
    CLLocationCoordinate2D touchMapCoordinate =
    [self.mapView convertPoint:touchPoint toCoordinateFromView:self.mapView];
    
    [self.mapView removeAnnotations:self.mapView.annotations];
    BSAnnotation *annot = [[BSAnnotation alloc] init];
    annot.coordinate = touchMapCoordinate;
    self.changePin.latitude = @(touchMapCoordinate.latitude);
    self.changePin.longitude = @(touchMapCoordinate.longitude);
    [self.mapView addAnnotation:annot];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"ChangePin"]) {
        BSImageViewController *vc = segue.destinationViewController;
        vc.picture = self.changePin;
    }
}

@end
