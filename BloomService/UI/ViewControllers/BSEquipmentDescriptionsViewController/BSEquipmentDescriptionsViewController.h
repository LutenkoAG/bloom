//
//  BSEquipmentDescriptionsViewController.h
//  BloomService
//
//  Created by Ruslan Shevtsov on 7/1/16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSDescroption.h"

@protocol BSEquipmentDescriptionsViewControllerDelegate <NSObject>

@required

- (void)selectedDescription:(BSDescroption *)description;

@end

@interface BSEquipmentDescriptionsViewController : UIViewController

@property (nonatomic, copy) NSArray *descriptions;
@property (nonatomic, assign) id<BSEquipmentDescriptionsViewControllerDelegate> delegate;

@end
