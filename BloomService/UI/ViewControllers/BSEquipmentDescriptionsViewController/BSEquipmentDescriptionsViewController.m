//
//  BSEquipmentDescriptionsViewController.m
//  BloomService
//
//  Created by Ruslan Shevtsov on 7/1/16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import "BSEquipmentDescriptionsViewController.h"

@interface BSEquipmentDescriptionsViewController () <UISearchBarDelegate>

@property (nonatomic, copy) NSArray *search;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation BSEquipmentDescriptionsViewController

- (void)setDescriptions:(NSArray *)descriptions {
    _descriptions = descriptions;
    self.search = descriptions;
}


#pragma mark -
#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.delegate) {
        BSDescroption *currentDescription = self.search[indexPath.row];
        [self.delegate selectedDescription:currentDescription];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark -
#pragma mark UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.search count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *kCellIdentifier = @"BSTableEquipmentDescriptionCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier];
    
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:kCellIdentifier];
    }
    
    BSDescroption *currentDescription = self.search[indexPath.row];
    
    cell.textLabel.text = currentDescription.text;
    
    return cell;
}

#pragma mark -
#pragma mark IBActions

- (IBAction)backButton:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark -
#pragma mark UISearchBarDelegate

- (BOOL)searchBar:(UISearchBar *)searchBar shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    NSString *searchString = nil;
    if (text.length) {
        searchString = [NSString stringWithFormat:@"%@%@", searchBar.text, text];
    } else {
        searchString = [searchBar.text substringToIndex:[searchBar.text length] - 1];
    }
    
    if (!searchString.length) {
        self.search = self.descriptions;
    } else {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"text contains[c] %@", searchString];
        self.search = [self.descriptions filteredArrayUsingPredicate:predicate];
    }
    [self.tableView reloadData];
    return YES;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSString *searchString = searchBar.text;
    
    if (!searchString.length) {
        self.search = self.descriptions;
    } else {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"text contains[c] %@", searchString];
        self.search = [self.descriptions filteredArrayUsingPredicate:predicate];
    }
    [self.tableView reloadData];
}

@end
