//
//  BSLoginViewController.m
//  BloomService
//
//  Created by user on 23.03.16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import "BSLoginViewController.h"
#import "UIViewController+Alert.h"
#import "UIViewController+HUD.h"
#import "BSAddressViewController.h"
#import "UIViewController+HUD.h"

#import "BSBusinessFacade.h"

@interface BSLoginViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;


@end

@implementation BSLoginViewController



- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self textLabelVersion];

    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    self.nameTextField.delegate = self;
    self.passwordTextField.delegate = self;
    [self.view addGestureRecognizer:tap];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)textLabelVersion {
#ifdef BETA
    self.versionLabel.text =  @"BETA";
    self.nameTextField.text = @"";
    self.passwordTextField.text = @"";
#else
    self.versionLabel.text =  @"DEV";
    self.nameTextField.text = @"testtechuser";
    self.passwordTextField.text = @"bloompass1";
#endif
}

#pragma mark -
#pragma mark IBActions

- (IBAction)loginAction:(id)sender {
    [self login];
}

#pragma mark -
#pragma mark UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.nameTextField) {
        [self.passwordTextField becomeFirstResponder];
    } else {
        [self dismissKeyboard];
        [self login];
    }
    return YES;
}

#pragma mark -
#pragma mark Login

- (void)login {
    NSString *email = self.nameTextField.text;
    NSString *pass = self.passwordTextField.text;
    
    if (email.length == 0 || pass.length == 0) {
        [self showAlertOkWithMessage:@"Please, input info!"];
        return;
    }
    
    [self showHUD];
    [BSBusinessFacade authorizationWithName:email password:pass completion:^(BOOL isSuccess, NSError *error) {
        if (isSuccess == NO) {
            [self showAlertError:error];
            [self hideHUD];
            return;
        } else {
                    [self hideHUD];
                    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
                    UINavigationController *navC = [story instantiateViewControllerWithIdentifier:@"BSAddressNavigationViewController"];
                    [[UIApplication sharedApplication].keyWindow setRootViewController:navC];
        }
    }];
}


#pragma mark - dismissKeyboard

-(void)dismissKeyboard {
    [_nameTextField resignFirstResponder];
    [_passwordTextField resignFirstResponder];
}
@end
