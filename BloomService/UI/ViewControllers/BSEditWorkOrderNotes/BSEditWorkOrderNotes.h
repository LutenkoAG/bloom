//
//  BSEditWorkOrderNotes.h
//  BloomService
//
//  Created by Aleksandr Lutenko on 23.08.16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BSWorkOrderComment.h"

typedef NS_ENUM(NSInteger, CommentMode) {
    CommentModeEdit,
    CommentModeAdd
};

@interface BSEditWorkOrderNotes : UIViewController

@property (nonatomic, strong) BSWorkOrderComment *editComment;
@property (nonatomic, assign) CommentMode mode;

@end
