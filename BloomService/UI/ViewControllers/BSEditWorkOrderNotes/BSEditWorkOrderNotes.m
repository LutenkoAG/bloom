//
//  BSEditWorkOrderNotes.m
//  BloomService
//
//  Created by Aleksandr Lutenko on 23.08.16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import "BSEditWorkOrderNotes.h"
#import "UIViewController+HUD.h"
#import "UIViewController+Alert.h"
#import "BSBusinessFacade.h"
#import "BSWorkOrderComment.h"

@interface BSEditWorkOrderNotes () <UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet UITextField *noteTextField;
@property (strong, nonatomic) IBOutlet UITextField *titleTextField;
@property (strong, nonatomic) NSString *workOrderNotes;

@end

@implementation BSEditWorkOrderNotes

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(dismissKeyboard)];
    
    [self.view addGestureRecognizer:tap];
    [self.titleTextField setDelegate:self];
    [self.noteTextField setDelegate:self];
    self.noteTextField.text = self.workOrderNotes;
    
    if (self.mode == CommentModeEdit){
        self.noteTextField.text = self.editComment.notes;
        self.titleTextField.text = self.editComment.titel;
    } else {
        self.noteTextField.text = @"";
        self.titleTextField.text =@"";
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == self.titleTextField) {
        [self.noteTextField becomeFirstResponder];
    } else {
        [self dismissKeyboard];
    }
    return YES;
}

- (IBAction)backButtonAction:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)saveButtonAction:(id)sender {
    BSWorkOrderComment *note = [[BSWorkOrderComment alloc] init];
    note.notes = self.noteTextField.text;
    note.titel = self.titleTextField.text;
    
    if (self.mode == CommentModeEdit){
        note.noteId = self.editComment.noteId;
        [self showHUD];
        [BSBusinessFacade editWorkOrderNotes:note
                                 completion:^(BOOL isSuccess, NSError *error) {
                                     [self hideHUD];
                                     if (error) {
                                         [self showAlertError:error];
                                     } else {
                                         [BSBusinessFacade updateWorkorders];
                                         [self.navigationController popViewControllerAnimated:YES];
                                     }
                                 }];
    } else {
        note.noteId = @(0);
        [self showHUD];
        [BSBusinessFacade addWorkOrderNotes:note
                                 completion:^(BOOL isSuccess, NSError *error) {
                                     [self hideHUD];
                                     if (error) {
                                         [self showAlertError:error];
                                     } else {
                                         [BSBusinessFacade updateWorkorders];
                                         [self.navigationController popViewControllerAnimated:YES];
                                     }
                                 }];
    }
}

-(void)dismissKeyboard {
    [self.titleTextField resignFirstResponder];
    [self.noteTextField resignFirstResponder];
}
    
@end
