//
//  BSAddressViewController.m
//  BloomService
//
//  Created by user on 23.03.16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import "BSAddressViewController.h"
#import "MapKit/MapKit.h"
#import "BSTableAddressCell.h"
#import <CoreLocation/CoreLocation.h>
#import "BSAnnotation.h"
#import "BSEquipment.h"

#import "BSBusinessFacade.h"
#import "BSDataManager.h"
#import "BSWorkOrder.h"
#import "BSLocationService.h"

static NSString * const kShowDetailsScreenIdentifier = @"ShowDetailsScreenIdentifier";

@interface BSAddressViewController () <UITableViewDataSource,
                                       UITableViewDelegate,
                                       MKMapViewDelegate,
                                       CLLocationManagerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *addressTableView;
@property (weak, nonatomic) IBOutlet UIView *mapAndDateView;
@property (weak, nonatomic) IBOutlet UIView *tableAndSortView;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segment;
@property (strong, nonatomic) NSDateFormatter *dateFormatter;
@property (weak, nonatomic) IBOutlet UISegmentedControl *sortSegment;
@property (assign, nonatomic) NSInteger segmentIndex;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (weak, nonatomic) IBOutlet UIView *datePickerView;
@property (strong, nonatomic) NSDateFormatter *dateFormatterMap;
@property (weak, nonatomic) IBOutlet UIButton *okButton;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;

@property (assign, nonatomic) BOOL reversDataSource;

@property int k;
@property int n;
@property int m;

@end

@implementation BSAddressViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.k = 1;
    self.n = 1;
    self.m = 1;
    
    [[self.okButton layer] setBorderWidth:1.0f];
    [[self.cancelButton layer] setBorderWidth:1.0f];
    
    self.dateFormatter = [[NSDateFormatter alloc] init];
    [self.dateFormatter setDateFormat:@"MM/dd/YYYY hh:mm a"];
    self.dateFormatterMap = [[NSDateFormatter alloc] init];
    [self.dateFormatterMap setDateFormat:@"MM/dd/YYYY"];

    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:20.0f]};
    [self.sortSegment setTitleTextAttributes:attributes
                            forState:UIControlStateNormal];
    [self.segment setTitleTextAttributes:attributes
                                    forState:UIControlStateNormal];
    
    self.addressTableView.delegate = self;
    self.addressTableView.dataSource = self;
    self.mapView.delegate = self;
    self.mapView.showsUserLocation = YES;
    
    NSDate *currentDate = [NSDate date];
    [self.datePicker setDate:currentDate];
    self.dateLabel.text = [self.dateFormatterMap stringFromDate:currentDate];
    
    self.workOrders = [BSBusinessFacade dataManager].workorders;
    
    [self reloadMapAnnotations];
    
    [self.sortSegment setTitle:@"▼ Time" forSegmentAtIndex:0];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.navigationController.navigationBarHidden = NO;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUI:) name:kGetWorkordersKey object:nil];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)updateUI:(NSNotification *)notification {
    [self.addressTableView reloadData];
    [self reloadMapAnnotations];
}

- (IBAction)okButton:(id)sender {
    self.datePickerView.hidden = YES;
    self.dateLabel.text = [self.dateFormatterMap stringFromDate:self.datePicker.date];
    [self reloadMapAnnotations];
}

- (IBAction)cancelButton:(id)sender {
    self.datePickerView.hidden = YES;
}

- (IBAction)SelectDateButton:(id)sender {
    self.datePickerView.hidden = NO;
}

- (IBAction)logOutAction:(id)sender {
    UIStoryboard *story = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    UINavigationController *navC = [story instantiateViewControllerWithIdentifier:@"navBSLoginViewController"];
    [[UIApplication sharedApplication].keyWindow setRootViewController:navC];
    [BSBusinessFacade unauthorization];
}

#pragma mark - segmented control

- (IBAction) segmentControlAction:(id)sender {
    UISegmentedControl* control = sender ;
    if([control selectedSegmentIndex] == 0) {
        [ self.view bringSubviewToFront:self.tableAndSortView];
    }
    if([control selectedSegmentIndex] == 1) {
        [ self.view bringSubviewToFront:self.mapAndDateView] ;
    }
}

#pragma mark - sort segmented control

- (IBAction)sortSegmentControlAction:(id)sender {
    UISegmentedControl *control = sender;
    if([control selectedSegmentIndex] == 0) {
        if (self.segmentIndex != 0) {
        self.segmentIndex = 0;
        self.reversDataSource = NO;
        [self.sortSegment setTitle:@"▲ Time" forSegmentAtIndex:0];
        [self.sortSegment setTitle:@"Customer" forSegmentAtIndex:1];
        [self.sortSegment setTitle:@"Address" forSegmentAtIndex:2];
    } else {
        self.reversDataSource = !self.reversDataSource;
        if (self.k == 1) {
        [self.sortSegment setTitle:@"▼ Time" forSegmentAtIndex:0];
        [self.sortSegment setTitle:@"Customer" forSegmentAtIndex:1];
        [self.sortSegment setTitle:@"Address" forSegmentAtIndex:2];
            self.k++;
        } else {
            [self.sortSegment setTitle:@"▲ Time" forSegmentAtIndex:0];
            [self.sortSegment setTitle:@"Customer" forSegmentAtIndex:1];
            [self.sortSegment setTitle:@"Address" forSegmentAtIndex:2];
            self.k=1;
        }
    }
    }
    if([control selectedSegmentIndex] == 1) {
        if (self.segmentIndex != 1) {
        self.segmentIndex = 1;
        self.reversDataSource = NO;
        [self.sortSegment setTitle:@"Time" forSegmentAtIndex:0];
        [self.sortSegment setTitle:@"▲ Customer" forSegmentAtIndex:1];
        [self.sortSegment setTitle:@"Address" forSegmentAtIndex:2];
    } else {
        self.reversDataSource = !self.reversDataSource;
        if (self.n == 1) {
        [self.sortSegment setTitle:@"▼ Customer" forSegmentAtIndex:1];
        [self.sortSegment setTitle:@"Time" forSegmentAtIndex:0];
        [self.sortSegment setTitle:@"Address" forSegmentAtIndex:2];
            self.n++;
        } else {
            [self.sortSegment setTitle:@"Time" forSegmentAtIndex:0];
            [self.sortSegment setTitle:@"▲ Customer" forSegmentAtIndex:1];
            [self.sortSegment setTitle:@"Address" forSegmentAtIndex:2];
            self.n=1;
        }
    }
    }
    if([control selectedSegmentIndex] == 2) {
        if (self.segmentIndex != 2) {
        self.segmentIndex = 2;
        self.reversDataSource = NO;
        [self.sortSegment setTitle:@"Time" forSegmentAtIndex:0];
        [self.sortSegment setTitle:@"Customer" forSegmentAtIndex:1];
        [self.sortSegment setTitle:@"▲ Address" forSegmentAtIndex:2];
    } else {
        self.reversDataSource = !self.reversDataSource;
        if (self.m == 1) {
        [self.sortSegment setTitle:@"▼ Address" forSegmentAtIndex:2];
        [self.sortSegment setTitle:@"Time" forSegmentAtIndex:0];
        [self.sortSegment setTitle:@"Customer" forSegmentAtIndex:1];
            self.m++;
        } else {
            [self.sortSegment setTitle:@"Time" forSegmentAtIndex:0];
            [self.sortSegment setTitle:@"Customer" forSegmentAtIndex:1];
            [self.sortSegment setTitle:@"▲ Address" forSegmentAtIndex:2];
            self.m=1;
        }
    }
    }
    [self.addressTableView reloadData];
}

#pragma mark MapView Delegate

- (void)reloadMapAnnotations {
    NSArray *workorders = [BSBusinessFacade dataManager].workorders;
   
    [self.mapView removeAnnotations:self.mapView .annotations];
    [workorders enumerateObjectsUsingBlock:^(BSWorkOrder *workorder, NSUInteger idx, BOOL * _Nonnull stop) {
        if ([self.dateLabel.text isEqualToString:[self.dateFormatterMap stringFromDate:workorder.time]]) {
        if (workorder.latitude && workorder.longitude) {
                BSAnnotation *pin = [[BSAnnotation alloc] init];
                pin.workOrder = workorder;
                pin.title = workorder.customer;
                pin.subtitle = [NSString stringWithFormat:@"%@"@", "@"%@"@", "@"%@", workorder.problem, workorder.location, workorder.address];
                pin.coordinate = CLLocationCoordinate2DMake([workorder.latitude doubleValue], [workorder.longitude doubleValue]);
            
                [self.mapView addAnnotation:pin];
            }
        }
    }];
}

- (nullable MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    if ([annotation isKindOfClass:[MKUserLocation class]]) {
        
        static NSString * customAnnotationIdentifier = @"annotationViewIdentifier";
        MKAnnotationView  *annotationView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:customAnnotationIdentifier];
        if (!annotationView) {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:nil];
        }
        annotationView.canShowCallout = YES;
        annotationView.image = [UIImage imageNamed:@"technician2"];
        return annotationView;;
    }
    static NSString * customAnnotationIdentifier = @"annotationViewIdentifier";
    MKAnnotationView  *annotationView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:customAnnotationIdentifier];
    if (!annotationView) {
        annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:nil];
    }
    annotationView.canShowCallout = YES;
    annotationView.image = [UIImage imageNamed:@"workorder1"];
    return annotationView;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    
    for (BSAnnotation *annotation in mapView.annotations) {
        MKAnnotationView *annotationView = [mapView viewForAnnotation:annotation];
        for (UIGestureRecognizer *recognizer in annotationView.gestureRecognizers) {
            [view removeGestureRecognizer:recognizer];
        }
    }
    
    
    UITapGestureRecognizer *colloutTapGesture =
    [[UITapGestureRecognizer alloc] initWithTarget:self
                                            action:@selector(calloutTapped:)];
    [view addGestureRecognizer:colloutTapGesture];
}

- (void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view {
    for (UIGestureRecognizer *recognizer in view.gestureRecognizers) {
        [view removeGestureRecognizer:recognizer];
    }
}

- (void)calloutTapped:(UITapGestureRecognizer *)sender {
    
    BSAnnotation *annotation = ((MKAnnotationView *)sender.view).annotation;
    if ([annotation isKindOfClass:[MKUserLocation class]]) return;
    [BSBusinessFacade cacheWorkOrder:annotation.workOrder];
    [self performSegueWithIdentifier:kShowDetailsScreenIdentifier sender:self];
}

#pragma mark -
#pragma mark UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSUInteger index =  self.reversDataSource == NO ? indexPath.row : ([[BSBusinessFacade dataManager].workorders count] - indexPath.row - 1);
    BSWorkOrder *currentWorkOrder = [[BSBusinessFacade dataManager] workordersBySortDescription:self.segmentIndex][index];
    [BSBusinessFacade cacheWorkOrder:currentWorkOrder];
//    [self performSegueWithIdentifier:kShowDetailsScreenIdentifier sender:self];
}

#pragma mark - UITableViewDataSource

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 70;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [[BSBusinessFacade dataManager].workorders count];

}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    static NSString *CellIdentifier = @"BSTableAddressCell";
    BSTableAddressCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil) {
        
        cell = [[BSTableAddressCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    NSInteger segmentControlIndex = self.segmentIndex;
    NSArray *workOrders = [[BSBusinessFacade dataManager] workordersBySortDescription:segmentControlIndex];
    NSUInteger index =  self.reversDataSource == NO ? indexPath.row : ([[BSBusinessFacade dataManager].workorders count] - indexPath.row - 1);
    BSWorkOrder *currentWorkOrder = workOrders[index];
    
    cell.locationLabel.text = currentWorkOrder.location;
    cell.customerLabel.text = [NSString stringWithFormat:@"%@"@"-"@"%@",currentWorkOrder.customer, currentWorkOrder.job];
    cell.timeLabel.text = [_dateFormatter stringFromDate:currentWorkOrder.time];
    cell.numberWorkorder.text = [NSString stringWithFormat:@"%@", currentWorkOrder.workorder];
    cell.addressLabel.text = currentWorkOrder.address;
   
    UILongPressGestureRecognizer *longPressgesture = [[UILongPressGestureRecognizer alloc] initWithTarget:self action:@selector(handleLongPress:)];
    [longPressgesture setMinimumPressDuration:2.0];
    [cell addGestureRecognizer:longPressgesture];
    
    return cell;
}

-(void)handleLongPress:(UILongPressGestureRecognizer *)gestureRecognizer {
    CGPoint p = [gestureRecognizer locationInView:self.addressTableView];
    
    NSIndexPath *indexPath = [self.addressTableView indexPathForRowAtPoint:p];
    if (indexPath == nil)
        NSLog(@"long press on table view but not on a row");
    else if (gestureRecognizer.state == UIGestureRecognizerStateEnded) {
        NSLog(@"UIGestureRecognizerStateEnded");
        //Do Whatever You want on End of Gesture
    } else if (gestureRecognizer.state == UIGestureRecognizerStateBegan) {
        
        NSInteger segmentControlIndex = self.segmentIndex;
        NSArray *workOrders = [[BSBusinessFacade dataManager] workordersBySortDescription:segmentControlIndex];
        NSUInteger index =  self.reversDataSource == NO ? indexPath.row : ([[BSBusinessFacade dataManager].workorders count] - indexPath.row - 1);
        BSWorkOrder *currentWorkOrder = workOrders[index];
        self.dateLabel.text =[self.dateFormatterMap stringFromDate:currentWorkOrder.time];
        self.segment.selectedSegmentIndex = 1;
        
        double lat = [currentWorkOrder.latitude doubleValue];
        double lon = [currentWorkOrder.longitude doubleValue];
        CLLocationCoordinate2D startCoord = CLLocationCoordinate2DMake(lat, lon);
        MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:MKCoordinateRegionMakeWithDistance(startCoord, 200000, 200000)];
        [self.mapView setRegion:adjustedRegion animated:YES];
        [ self.view bringSubviewToFront:self.mapAndDateView];
    }
}

@end

