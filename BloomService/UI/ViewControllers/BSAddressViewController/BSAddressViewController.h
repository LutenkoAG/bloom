//
//  BSAddressViewController.h
//  BloomService
//
//  Created by user on 23.03.16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSAddressViewController : UIViewController

@property (strong, nonatomic) NSArray *workOrders;

@end
