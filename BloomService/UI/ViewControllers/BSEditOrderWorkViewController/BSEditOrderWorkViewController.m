//
//  BSEditOrderWorkViewController.m
//  BloomService
//
//  Created by user on 23.03.16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import "BSEditOrderWorkViewController.h"
#import "UIViewController+ImagePiker.h"
#import "BSStatusTableViewController.h"
#import "BSWorkOrder.h"
#import "BSBusinessFacade.h"
#import "BSEquipment.h"
#import "UIViewController+HUD.h"

#import "BSAddOneEqyupment.h"
#import "BSImage.h"
#import "BSEditWorkOrderNotes.h"
#import "BSPartsOrLabor.h"
#import "BSLocationService.h"

// UI
#import "BSEdetiWorkOrderEquipmentCell.h"
#import "BSImageViewController.h"
#import "UIImageView+AFNetworking.h"
#import "UIViewController+Alert.h"
#import "BSRoundButton.h"
#import "CollectionCell.h"
#import "PWProgressView.h"
#import "MapKit/MapKit.h"
#import "BSAnnotation.h"
#import <CoreLocation/CoreLocation.h>
#import "BSLocationService.h"
#import "BSWorcOrderNotesCell.h"
#import "BSWorkOrderComment.h"

static NSString * const kShowEditImage = @"ShowEditImage";


@interface BSEditOrderWorkViewController ()    <BSEdetiWorkOrderEquipmentCellDelegate,
                                                BSEdetiWorkOrderCommentCellDelegate,
                                                CollectionCellDelegate,
                                                UIImagePickerControllerDelegate,
                                                UINavigationControllerDelegate,
                                                UITableViewDataSource,
                                                UITableViewDelegate,
                                                UICollectionViewDelegate,
                                                UICollectionViewDataSource,
                                                MKMapViewDelegate,
                                                CLLocationManagerDelegate>

@property (weak, nonatomic) IBOutlet UIButton *addButton;
@property (weak, nonatomic) IBOutlet UIButton *attachPictureButton;
@property (weak, nonatomic) IBOutlet UITableView *equipmentTableView;
@property (weak, nonatomic) IBOutlet UICollectionView *pictureCollectionView;
@property (weak, nonatomic) IBOutlet UIButton *statusButton;
@property (weak, nonatomic) IBOutlet UISegmentedControl *segmentControl;
@property (weak, nonatomic) IBOutlet UIView *picturesView;
@property (weak, nonatomic) IBOutlet UIView *partsAndLaborView;
@property (weak, nonatomic) IBOutlet UIView *infoView;
@property (weak, nonatomic) IBOutlet UIView *workOrderNotesView;

@property (weak, nonatomic) IBOutlet UITableView *workOrderNotesTableView;

@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UILabel *customerLabel;
@property (weak, nonatomic) IBOutlet UILabel *problemLabel;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UILabel *doNotExeed;
@property (weak, nonatomic) IBOutlet UILabel *locationNotesLabel;
@property (weak, nonatomic) IBOutlet UILabel *contact;
@property (weak, nonatomic) IBOutlet UILabel *equipment;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *locationNotesHeight;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *locationNotesTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mapViewTopConstraint;
@property (weak, nonatomic) IBOutlet UIView *workOrderNoteSection;
@property (weak, nonatomic) IBOutlet UIView *locationNotesSection;

@property (weak, nonatomic) NSString *longStringLocationNotes;
@property (weak, nonatomic) NSString *strLocationNotes;
@property (weak, nonatomic) NSString *longStringWorkOrderNotes;
@property (weak, nonatomic) NSString *strWorcOrderNotes;
@property (strong, nonatomic) NSMutableArray *collectionImages;
@property (strong, nonatomic) BSImage *picture;
@property (strong, nonatomic) BSWorkOrder *currentWorkOrder;
@property (nonatomic, strong) UIImage *image;

@end

@implementation BSEditOrderWorkViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.equipmentTableView.tableFooterView = [UIView new];
    
    self.collectionImages = [[NSMutableArray alloc] init];

    NSDictionary *attributes = @{NSFontAttributeName: [UIFont boldSystemFontOfSize:20.0f]};
    [self.segmentControl setTitleTextAttributes:attributes
                                       forState:UIControlStateNormal];
    
    self.currentWorkOrder = [BSBusinessFacade getCachedWorkOrder];
    [self.collectionImages addObjectsFromArray:self.currentWorkOrder.pictures];
    if (self.collectionImages.count == 0){
        [self showAlertOkWithMessage:@"Remember, you MUST take a photo of the front of the building before saving."];
    }
    
    self.mapView.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self setupStatusButton:self.currentWorkOrder.status];
    self.locationLabel.text   = self.currentWorkOrder.location;
    self.problemLabel.text    = self.currentWorkOrder.problem;
    self.customerLabel.text   = self.currentWorkOrder.customer;
    self.doNotExeed.text = self.currentWorkOrder.NottoExceed;
    self.contact.text = self.currentWorkOrder.contact;
    BSEquipment *equip = [self.currentWorkOrder.equipmentType firstObject];
    self.equipment.text = equip.equipmentType;
    self.addressLabel.text = self.currentWorkOrder.address;

    self.strLocationNotes = self.currentWorkOrder.locationNotes;
    self.strLocationNotes =[self.strLocationNotes stringByReplacingOccurrencesOfString:@"\r\n" withString:@" "];
    self.locationNotesLabel.text = self.strLocationNotes;
    self.longStringLocationNotes = self.locationNotesLabel.text;
    CGSize lableSize = CGSizeMake(self.locationNotesLabel.frame.size.width - 20, CGFLOAT_MAX);
    CGRect requiredRect = [self.longStringLocationNotes boundingRectWithSize:lableSize options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:17]} context:nil];
    if (requiredRect.size.height <= 50) {
        self.locationNotesHeight.constant = 50;
    } else {
        self.locationNotesHeight.constant = requiredRect.size.height;
        self.locationNotesLabel.textAlignment = NSTextAlignmentLeft;
    }
    self.navigationItem.title = [NSString stringWithFormat:@"%@%@", self.currentWorkOrder.workorder ,@" Info"];
    
    if ([self.doNotExeed.text isEqualToString:@""]){
        self.locationNotesTopConstraint.constant = -50;
//        self.workOrderNoteSection.hidden = YES;
    }
    if ([self.locationNotesLabel.text isEqualToString:@""]){
        self.mapViewTopConstraint.constant = -43;
//        self.locationNotesSection.hidden = YES;
    }
    
    [self reloadMapAnnotations];
    [self.equipmentTableView reloadData];
    [self.workOrderNotesTableView reloadData];
    
}

- (IBAction)openGoogleMapsAction:(id)sender {
    
//    BSLocation *location = [BSBusinessFacade currentLocation];
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"comgooglemaps://?daddr=%@,%@&directionsmode=driving",
                          [self.currentWorkOrder.latitude stringValue], [self.currentWorkOrder.longitude stringValue]]];
//    if ([[UIApplication sharedApplication] canOpenURL:
//         [NSURL URLWithString:@"comgooglemaps://"]]) {
        [[UIApplication sharedApplication] openURL:url];
//    } else {
//        [self showAlertOkWithTitie:@"Info" message:@"Google Maps app need to be installed on device"];
//    }
}


#pragma mark MapView Delegate

- (void)reloadMapAnnotations {
    [self.mapView removeAnnotations:self.mapView .annotations];
    self.currentWorkOrder     = [BSBusinessFacade getCachedWorkOrder];
    if (self.currentWorkOrder.latitude && self.currentWorkOrder.longitude) {
        BSAnnotation *pin = [[BSAnnotation alloc] init];
        pin.title = self.currentWorkOrder.customer;
        pin.subtitle = [NSString stringWithFormat:@"%@"@", "@"%@", self.currentWorkOrder.problem, self.currentWorkOrder.location];
        pin.coordinate = CLLocationCoordinate2DMake([self.currentWorkOrder.latitude doubleValue], [self.currentWorkOrder.longitude doubleValue]);
        [self.mapView addAnnotation:pin];
        MKCoordinateRegion adjustedRegion = [self.mapView regionThatFits:MKCoordinateRegionMakeWithDistance(pin.coordinate, 20000, 20000)];
        [self.mapView setRegion:adjustedRegion animated:YES];
    }
}

- (nullable MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id <MKAnnotation>)annotation {
    
    if ([annotation isKindOfClass:[BSAnnotation class]])
    {
        static NSString * customAnnotationIdentifier = @"annotationViewIdentifier";
        MKAnnotationView  *annotationView = (MKAnnotationView *)[mapView dequeueReusableAnnotationViewWithIdentifier:customAnnotationIdentifier];
        if (!annotationView) {
            annotationView = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:nil];
        }
        //        annotationView.canShowCallout = YES;
        annotationView.image = [UIImage imageNamed:@"workorder1"];
        return annotationView;
    }
    return nil;
}

- (void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view {
    
    BSLocation *currentLocation = [BSBusinessFacade currentLocation];
    
    NSString *strapplemap = [NSString stringWithFormat:@"http://maps.apple.com/?saddr="@"%f"@","@"%f&daddr="@"%f"@","@"%f&mode=d", currentLocation.latitude, currentLocation.longitude, [self.currentWorkOrder.latitude doubleValue], [self.currentWorkOrder.longitude doubleValue]];
    
    NSURL *URL = [NSURL URLWithString:strapplemap];
    [[UIApplication sharedApplication] openURL:URL];
    
    for(BSAnnotation *annotation in mapView.selectedAnnotations) {
        [mapView deselectAnnotation:annotation animated:NO];
    }
}

#pragma mark -

- (void)setupStatusButton:(NSString *)status {
    
    [self.statusButton setTitle:status forState:UIControlStateNormal];
    if ([status  isEqualToString: @"Open"]) {
        [self.statusButton setBackgroundColor:[UIColor colorWithRed:229.0/255.0 green:0.0/255.0 blue:28.0/255.0 alpha:1.0]];
    } else if ([status  isEqualToString: @"Work Complete"]) {
        [self.statusButton setBackgroundColor:[UIColor colorWithRed:114.0/255.0 green:131.0/255.0 blue:38.0/255.0 alpha:1.0]];
    } else if ([status  isEqualToString: @"Return Required"]) {
        [self.statusButton setBackgroundColor:[UIColor colorWithRed:240.0/255.0 green:109.0/255.0 blue:25.0/255.0 alpha:1.0]];
    } else if ([status  isEqualToString: @"Close"]) {
        [self.statusButton setBackgroundColor:[UIColor colorWithRed:23.0/255.0 green:167.0/255.0 blue:31.0/255.0 alpha:1.0]];
    }
}

- (IBAction)backButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)attachPhotoAction:(id)sender {
    [self showImagePiker:^(BOOL isDone, UIImage *image, NSError *anError) {
            if (isDone) {
               
                BSImage *bsImage = [BSImage new];
                bsImage.image = image;
                bsImage.isUploading = YES;
                if (self.collectionImages == nil){
                    bsImage.idImage = @(1);
                } else {
                BSImage *img = [self.collectionImages lastObject];
                bsImage.idImage = @([img.idImage intValue] + 1);
                }
                NSMutableArray *pictures = [NSMutableArray arrayWithArray:self.currentWorkOrder.pictures];
                [pictures addObject:bsImage];
                self.currentWorkOrder.pictures = pictures;
                
                [self.collectionImages addObject:bsImage];
                [self.pictureCollectionView reloadData];
                
                NSUInteger index = [self.collectionImages indexOfObject:bsImage];
                NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];

                [BSBusinessFacade uploadImage:bsImage progress:^(float progress) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        CollectionCell *cell = (CollectionCell *)[self.pictureCollectionView cellForItemAtIndexPath:indexPath];
                        cell.progressView.progress = progress;
                        bsImage.progress = progress;
                    });
                } completion:^(BOOL isSuccess, NSError *error) {
                    
                    __block UIImage *blockImg = image;
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        if (error) {
                            
                            UIImageWriteToSavedPhotosAlbum(blockImg, nil, nil, nil);
                            [self showAlertError:error];
                        } else {
                            bsImage.isUploading = NO;
                            CollectionCell *cell = (CollectionCell *)[self.pictureCollectionView cellForItemAtIndexPath:indexPath];
                            [cell.progressView removeFromSuperview];
                            [BSBusinessFacade updateWorkorders];
                        }
                    });
                }];
                
            }
        }];
}

- (void)deleteImage:(BSImage *)image forCell:(CollectionCell *)cell {
    __weak __typeof(self)weakSelf = self;
    [cell.activityIndicatorView startAnimating];
    [BSBusinessFacade deleteImage:image completion:^(BOOL isSuccess, NSError *error) {
        [cell.activityIndicatorView stopAnimating];
        if (isSuccess) {
            [BSBusinessFacade updateWorkorders];
            [weakSelf.collectionImages removeObject:image];
            [weakSelf.pictureCollectionView reloadData];
        } else {
            if ([error.localizedDescription isEqualToString:@"Workorder images not found"]) {
                [BSBusinessFacade updateWorkorders];
                [weakSelf.collectionImages removeObject:image];
                [weakSelf.pictureCollectionView reloadData];
            } else {
                [self showAlertError:error];
            }
        }
    }];
}

#pragma mark - CollectionCellDelegate

- (void)collectionCellDeleteImage:(CollectionCell *)cell {
    NSIndexPath *indexPath = [self.pictureCollectionView indexPathForCell:cell];
    BSImage *image = [self.collectionImages objectAtIndex:indexPath.row];
    [self deleteImage:image forCell:cell];
}

#pragma mark - segmented control

- (IBAction)segmentControlAction:(id)sender {
    UISegmentedControl *control = sender;
    if([control selectedSegmentIndex] == 0) {
        [ self.view bringSubviewToFront:_infoView] ;
    }
    if([control selectedSegmentIndex] == 1) {
        [ self.view bringSubviewToFront:_partsAndLaborView] ;
    }
    if([control selectedSegmentIndex] == 2) {
        [ self.view bringSubviewToFront:_picturesView] ;
    }
    if([control selectedSegmentIndex] == 3) {
        [ self.view bringSubviewToFront:_workOrderNotesView] ;
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
        if (tableView == self.equipmentTableView) {
        return self.currentWorkOrder.equipments.count;
    } else if (tableView == self.workOrderNotesTableView) {
        return self.currentWorkOrder.workOrderComments.count;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.equipmentTableView) {
        static NSString *CellIdentifier = @"Cell";
        BSEdetiWorkOrderEquipmentCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[BSEdetiWorkOrderEquipmentCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        BSPartsOrLabor *equipment = self.currentWorkOrder.equipments[indexPath.row];
        cell.delegate = self;
        [cell setupCellWithEquipment:equipment];
        return cell;
        
    } else if (tableView == self.workOrderNotesTableView) {
        static NSString *CellIdentifier = @"NotesCell";
        BSWorcOrderNotesCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        if (cell == nil) {
            cell = [[BSWorcOrderNotesCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        }
        BSWorkOrderComment *commet = self.currentWorkOrder.workOrderComments[indexPath.row];
        cell.delegate = self;
        cell.titleLabel.text = commet.titel;
        cell.notesLabel.text = commet.notes;
        return cell;
    }
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.equipmentTableView){
        BSAddOneEqyupment *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AddEquipment"];
        BSPartsOrLabor *item = self.currentWorkOrder.equipments[indexPath.row];
        vc.editEquipment = item;
        vc.mode = EquipmentModeEdit;
        [self.navigationController pushViewController:vc animated:YES];
    } else if (tableView == self.workOrderNotesTableView){
        BSEditWorkOrderNotes *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"EditComment"];
        BSWorkOrderComment *item = self.currentWorkOrder.workOrderComments[indexPath.row];
        vc.editComment = item;
        vc.mode = CommentModeEdit;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

#pragma mark - BSEdetiWorkOrderEquipmentCellDelegate

- (void)editWorkOrderEquipmentCellDeleteAction:(BSEdetiWorkOrderEquipmentCell *)cell {
    NSIndexPath *indexPath = [self.equipmentTableView indexPathForCell:cell];
    BSPartsOrLabor *equipmentToDelete = self.currentWorkOrder.equipments[indexPath.row];
    
    NSMutableArray *arrayEquipments = [NSMutableArray arrayWithArray:self.currentWorkOrder.equipments];
    [self showHUD];
    __weak __typeof(self)weakSelf = self;
    [BSBusinessFacade deleteEquipment:equipmentToDelete
                           completion:^(BOOL isSuccess, NSError *error) {
                               [weakSelf hideHUD];
                               if (isSuccess) {
                                   [arrayEquipments removeObject:equipmentToDelete];
                                   weakSelf.currentWorkOrder.equipments = [NSArray arrayWithArray: arrayEquipments];
                                   [weakSelf.equipmentTableView reloadData];
                               } else {
                                   [weakSelf showAlertError:error];
                               }
                           }];
}

#pragma mark - BSEdetiWorkOrderCommentCellDelegate

- (void)editWorkOrderCommentCellDeleteAction:(BSWorcOrderNotesCell *)cell {
    NSIndexPath *indexPath = [self.workOrderNotesTableView indexPathForCell:cell];
    BSWorkOrderComment *commentToDelete = self.currentWorkOrder.workOrderComments[indexPath.row];
    
    NSMutableArray *arrayComments = [NSMutableArray arrayWithArray:self.currentWorkOrder.workOrderComments];
    [self showHUD];
    __weak __typeof(self)weakSelf = self;
    [BSBusinessFacade deleteWorkOrderNotes:commentToDelete
                           completion:^(BOOL isSuccess, NSError *error) {
                               [weakSelf hideHUD];
                               if (isSuccess) {
                                   [arrayComments removeObject:commentToDelete];
                                   weakSelf.currentWorkOrder.workOrderComments = [NSArray arrayWithArray: arrayComments];
                                   [weakSelf.workOrderNotesTableView reloadData];
                               } else {
                                   [weakSelf showAlertError:error];
                               }
                           }];
}


#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return  self.collectionImages.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    static NSString *identifier = @"CollectionCell";
    
    CollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:identifier forIndexPath:indexPath];
    cell.delegate = self;
    BSImage *picture = [self.collectionImages objectAtIndex:indexPath.row];

    
    if (picture.image == nil) {
        [cell.imageView setImageWithURL:[NSURL URLWithString:picture.imageUrl] placeholderImage:[UIImage imageNamed:@"plaseholderImage"]];
    } else {
        cell.imageView.image = picture.image;
    }
    
    if (picture.isUploading) {
        PWProgressView *cellProgressView = cell.progressView;
        if (!cellProgressView) {
            cellProgressView = [[PWProgressView alloc] initWithFrame:cell.bounds];
        }
        cellProgressView.progress = picture.progress;
        [cell addSubview:cellProgressView];
        cell.progressView = cellProgressView;
    } else {
        [cell.progressView removeFromSuperview];
    }
    
    cell.numberOfPicture.text = [NSString stringWithFormat:@"%ld.", (long)indexPath.row + 1];
//    NSUInteger numberint = [picture.idImage intValue];
//    NSString *numberstr = [NSString stringWithFormat:@"%lu.", (unsigned long)numberint];
//    cell.numberOfPicture.text = numberstr;
    return cell;
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    BSImage *item = self.collectionImages[indexPath.item];
    self.picture = item;
    return YES;
}

#pragma mark - StatusProtokol and EquipmentProtocol

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:@"ShowEditImage"]) {
        BSImageViewController *vc = segue.destinationViewController;
        vc.picture = self.picture;
    } else if ([segue.identifier isEqualToString:@"AddNewEquipment"]) {
        BSAddOneEqyupment *vc = segue.destinationViewController;
        vc.mode = EquipmentModeAdd;
    }
    else if ([segue.identifier isEqualToString:@"AddNewComment"]) {
        BSEditWorkOrderNotes *vc = segue.destinationViewController;
        vc.mode = CommentModeAdd;
    }
}

- (void)addEquipment:(NSMutableArray *)equipment {
    self.currentWorkOrder.equipments  = [NSMutableArray arrayWithArray:equipment];
    [self.equipmentTableView reloadData];
}

@end
