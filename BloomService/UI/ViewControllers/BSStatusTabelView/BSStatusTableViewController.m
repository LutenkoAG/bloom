//
//  BSStatusTableViewController.m
//  BloomService
//
//  Created by user on 24.03.16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import "BSStatusTableViewController.h"

// UI
#import "UIViewController+HUD.h"
#import "UIViewController+Alert.h"

// BS
#import "BSBusinessFacade.h"

@interface BSStatusTableViewController () <UITableViewDataSource, UITableViewDelegate>

@property (strong, nonatomic) IBOutlet UITableView *statusRefreshTable;
@property (strong,nonatomic) NSArray *statuses;

@end

@implementation BSStatusTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.statuses=[NSArray arrayWithObjects:@"Work Complete", @"Return Required", nil];
}

- (IBAction)backButtonClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.statuses.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"StatusCell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] ;
    }
    cell.textLabel.text=[self.statuses objectAtIndex:indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    NSString *status = [self.statuses objectAtIndex:indexPath.row];
    
    [self showHUD];
    
    [BSBusinessFacade updateStatus:status completion:^(BOOL isSuccess, NSError *error) {
        [self hideHUD];
        if (error) {
            [self showAlertError:error];
        } else {
            [BSBusinessFacade updateWorkorders];
            [self.navigationController popViewControllerAnimated:YES];
        }
    }];
}

@end
