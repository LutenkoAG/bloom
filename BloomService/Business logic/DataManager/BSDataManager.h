//
//  BSDataManager.h
//  BloomService
//
//  Created by Ruslan Shevtsov on 4/19/16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, WorkordersSortDescription) {
    WorkordersSortByDate = 0,
    WorkordersSortByCustomer,
    WorkordersSortByAddress
};

@interface BSDataManager : NSObject

@property (nonatomic, copy) NSArray *workorders;
@property (nonatomic, copy) NSArray *statuses;
@property (nonatomic, copy) NSArray *equipments;
@property (nonatomic, copy) NSArray *partDescriptions;
@property (nonatomic, copy) NSArray *repairDescriptions;
@property (nonatomic, strong) NSString *token;

- (NSArray *)workordersBySortDescription:(WorkordersSortDescription)sortDescription;

@end