//
//  BSDataManager.m
//  BloomService
//
//  Created by Ruslan Shevtsov on 4/19/16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import "BSDataManager.h"

// Entities
#import "BSWorkOrder.h"

@interface BSDataManager ()

@property (nonatomic, strong) NSArray *workordersByCustomer;
@property (nonatomic, strong) NSArray *workordersByLocation;

@end

@implementation BSDataManager

- (NSArray *)workordersBySortDescription:(WorkordersSortDescription)sortDescription {
    NSArray *sortedWorkorders = nil;
    switch (sortDescription) {
        case WorkordersSortByDate:
            sortedWorkorders = self.workorders;
            break;
            
        case WorkordersSortByCustomer:
            sortedWorkorders = self.workordersByCustomer;
            break;
            
        case WorkordersSortByAddress:
            sortedWorkorders = self.workordersByLocation;
            break;
    }
    
    return sortedWorkorders;
}

#pragma mark -
#pragma mark Setters

- (void)setWorkorders:(NSArray *)workorders {
    _workorders = workorders;
    _workordersByCustomer = [self.workorders sortedArrayUsingComparator: ^(BSWorkOrder *a1, BSWorkOrder *a2) {
        return [a1.customer compare:a2.customer];
    }];
    _workordersByLocation = [self.workorders sortedArrayUsingComparator: ^(BSWorkOrder *a1, BSWorkOrder *a2) {
        return [a1.location compare:a2.location];
    }];
}

@end