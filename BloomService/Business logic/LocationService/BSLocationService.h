//
//  BSLocationService.h
//  BloomService
//
//  Created by Ruslan Shevtsov on 4/21/16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BSLocation : NSObject

@property (nonatomic, assign) double longitude;
@property (nonatomic, assign) double latitude;

@end

@interface BSLocationService : NSObject

- (BSLocation *)location;

@end
