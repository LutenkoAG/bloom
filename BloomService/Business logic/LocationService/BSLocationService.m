//
//  BSLocationService.m
//  BloomService
//
//  Created by Ruslan Shevtsov on 4/21/16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import "BSLocationService.h"

#import <CoreLocation/CoreLocation.h>

static NSString * const kCurrentLocationLatitudeKey = @"CurrentLocationLatitude";
static NSString * const kCurrentLocationLongitudeKey = @"CurrentLocationLongitude";

@interface BSLocation ()

@end

@implementation BSLocation

- (instancetype)initWithCLLocation:(CLLocation *)clLocation {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    _longitude = clLocation.coordinate.longitude;
    _latitude = clLocation.coordinate.latitude;
    
    return self;
}

- (instancetype)initWithLongitude:(double)longitude latitude:(double)latitude {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    _longitude = longitude;
    _latitude = latitude;
    
    return self;
}

@end

@interface BSLocationService () <CLLocationManagerDelegate>

@property (nonatomic, strong) BSLocation *currentLocation;
@property (nonatomic, strong) CLLocationManager *locationManager;

@end

@implementation BSLocationService

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.locationManager = [[CLLocationManager alloc] init];
    
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    [self.locationManager requestWhenInUseAuthorization];
    
    if ([CLLocationManager locationServicesEnabled] == YES) {
        [self.locationManager startUpdatingLocation];
    }
    
    return self;
}

- (BSLocation *)location {
    BSLocation *location = self.currentLocation;
    if (location == nil) {
        NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
        double longitude = [userDefaults doubleForKey:kCurrentLocationLongitudeKey];
        double latitude = [userDefaults doubleForKey:kCurrentLocationLatitudeKey];
        location = [[BSLocation alloc] initWithLongitude:longitude latitude:latitude];
    }
    return location;
}

#pragma mark -
#pragma mark CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    self.currentLocation = [[BSLocation alloc] initWithCLLocation:newLocation];
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setDouble:self.currentLocation.latitude forKey:kCurrentLocationLatitudeKey];
    [userDefaults setDouble:self.currentLocation.longitude forKey:kCurrentLocationLongitudeKey];
    [userDefaults synchronize];
    
}

@end
