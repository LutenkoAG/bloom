//
//  BSEquipment.m
//  BloomService
//
//  Created by Aleksandr Lutenko on 03.06.16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import "BSPartsOrLabor.h"
#import "NSDictionary+Extended.h"

NSString * const kLabor = @"Labor";
NSString * const kParts = @"Parts";

@implementation BSPartsOrLabor


-(instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (self = [super init]) {
        self.name = [dictionary tryObjectForKey:@"Description"];
        self.count = [dictionary tryObjectForKey:@"Quantity"];
        self.type = [dictionary tryObjectForKey:@"ItemType"];
        self.rate = [dictionary tryObjectForKey:@"UnitSale"];
        self.amount = [dictionary tryObjectForKey:@"TotalSale"];
        self.repairDescription = [dictionary tryObjectForKey:@"LaborItem"];
        NSString *date = [dictionary tryObjectForKey:@"WorkDate"];
        date = [date stringByReplacingOccurrencesOfString:@"T" withString:@" "];
        date = [date stringByReplacingOccurrencesOfString:@"Z" withString:@""];
        NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
        [formatter1 setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
        self.date = [formatter1 dateFromString:date];
        self.equipmentId = [dictionary tryObjectForKey:@"WorkOrderItem"];
        self.workorderId = [dictionary tryObjectForKey:@"WorkOrder"];
        self.employee = [dictionary tryObjectForKey:@"Emploee"];
        self.part = [dictionary tryObjectForKey:@"Part"];
        self.costQty = [dictionary tryObjectForKey:@"CostQuantity"];
    }
    return self;
}

@end
