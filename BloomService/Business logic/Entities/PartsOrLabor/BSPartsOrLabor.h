//
//  BSEquipment.h
//  BloomService
//
//  Created by Aleksandr Lutenko on 03.06.16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BSDescroption.h"
#import "BSRepairDescription.h"
#import "BSPartDescription.h"


extern NSString * const kLabor;
extern NSString * const kParts;

@interface BSPartsOrLabor : NSObject

@property (strong, nonatomic) NSString *name;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSNumber *count;
@property (strong, nonatomic) NSNumber *rate;
@property (strong, nonatomic) NSDate *date;
@property (strong, nonatomic) NSString *equipmentId;
@property (strong, nonatomic) NSString *workorderId;
@property (strong, nonatomic) BSRepairDescription *repairDescription;
@property (strong, nonatomic) NSNumber *part;
@property (strong, nonatomic) NSNumber *costQty;
@property (strong, nonatomic) NSString *employee;
@property (strong, nonatomic) NSNumber *amount;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
