//
//  BSWorcOrderComment.m
//  BloomService
//
//  Created by Aleksandr Lutenko on 26.08.16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import "BSWorkOrderComment.h"
#import "NSDictionary+Extended.h"

@implementation BSWorkOrderComment

-(instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (self = [super init]) {
        self.titel = [dictionary tryObjectForKey:@"SUBJECTLINE"];
        self.notes = [dictionary tryObjectForKey:@"TEXT"];
        self.noteId = [dictionary tryObjectForKey:@"NOTENBR"];
    }
    return self;
}

@end
