//
//  BSWorcOrderComment.h
//  BloomService
//
//  Created by Aleksandr Lutenko on 26.08.16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BSWorkOrderComment : NSObject

@property (nonatomic, strong) NSString *titel;
@property (nonatomic, strong) NSString *notes;
@property (strong, nonatomic) NSNumber *noteId;
@property (strong, nonatomic) NSString *workorderId;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
