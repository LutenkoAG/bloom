//
//  BSWorkOrder.h
//  BloomService
//
//  Created by Ruslan Shevtsov on 4/19/16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BSWorkOrder : NSObject

@property (nonatomic, retain) NSString *customer;
@property (nonatomic, retain) NSString *status;
@property (nonatomic, retain) NSString *location;
@property (nonatomic, retain) NSString *problem;
@property (nonatomic, retain) NSDate *time;
@property (nonatomic, retain) NSArray *equipments;
@property (nonatomic, retain) NSArray *pictures;
@property (nonatomic, retain) NSNumber *latitude;
@property (nonatomic, retain) NSNumber *longitude;
@property (nonatomic, retain) NSString *workorder;
@property (nonatomic, retain) NSString *address;
@property (nonatomic, retain) NSString *job;
@property (nonatomic, retain) NSString *NottoExceed;
@property (nonatomic, retain) NSString *locationNotes;
@property (nonatomic, retain) NSArray *workOrderComments;
@property (nonatomic, retain) NSString *contact;
@property (nonatomic, retain) NSArray *equipmentType;



-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
