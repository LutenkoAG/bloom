//
//  BSWorkOrder.m
//  BloomService
//
//  Created by Ruslan Shevtsov on 4/19/16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import "BSWorkOrder.h"

#import <CoreLocation/CoreLocation.h>
#import "NSDictionary+Extended.h"
#import "BSPartsOrLabor.h"
#import "BSImage.h"
#import "BSWorkOrderComment.h"
#import "BSEquipment.h"


@implementation BSWorkOrder

-(instancetype)initWithDictionary:(NSDictionary *)dictionary{
    if (self = [super init]) {
        
        self.customer = [dictionary tryObjectForKey:@"ARCustomer"];
        self.status = [dictionary tryObjectForKey:@"Status"];
        self.location = [dictionary tryObjectForKey:@"Location"];
        self.address = [dictionary tryObjectForKey:@"Address"];
        self.problem = [dictionary tryObjectForKey:@"Problem"];
        NSString *time = [dictionary tryObjectForKey:@"ScheduleDate"];
        time = [time stringByReplacingOccurrencesOfString:@"T" withString:@" "];
        time = [time stringByReplacingOccurrencesOfString:@"Z" withString:@""];
        NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
        [formatter1 setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
        self.time = [formatter1 dateFromString:time];
        self.latitude = [dictionary tryObjectForKey:@"Latitude"];
        self.longitude = [dictionary tryObjectForKey:@"Longitude"];
        self.workorder = [dictionary tryObjectForKey:@"WorkOrder"];
        self.job = [dictionary tryObjectForKey:@"JCJob"];
        self.NottoExceed = [dictionary tryObjectForKey:@"NottoExceed"];
        self.locationNotes = [dictionary tryObjectForKey:@"Comments"];
        self.contact = [dictionary tryObjectForKey:@"Contact"];
        
        NSMutableArray *equipmentses = [NSMutableArray new];
        for (NSDictionary *equipmentDic in [dictionary tryObjectForKey:@"Equipments"]) {
            BSEquipment *equipment = [[BSEquipment alloc] initWithDictionary: equipmentDic];
            [equipmentses addObject:equipment];
        }
        self.equipmentType = equipmentses;
        
        NSMutableArray *equipments = [NSMutableArray new];
        for (NSDictionary *equipmentDict in [dictionary tryObjectForKey:@"WorkOrderItems"]) {
            BSPartsOrLabor *equipment = [[BSPartsOrLabor alloc] initWithDictionary: equipmentDict];
            [equipments addObject:equipment];
        }
        self.equipments = equipments;
        
        NSMutableArray *workOrderComments = [NSMutableArray new];
        for (NSDictionary *wOComment in [dictionary tryObjectForKey:@"WorkNotes"]) {
            BSWorkOrderComment *comment = [[BSWorkOrderComment alloc] initWithDictionary: wOComment];
            [workOrderComments addObject:comment];
        }
        self.workOrderComments = workOrderComments;
        
        NSMutableArray *pictures = [NSMutableArray new];
        for (NSDictionary *picturesDict in [dictionary tryObjectForKey:@"Images"]) {
            BSImage *image = [[BSImage alloc] initWithDictionary: picturesDict];
            
            [pictures addObject:image];
        }
        self.pictures = [pictures sortedArrayUsingComparator: ^(BSImage *a1, BSImage *a2) {
            return [a1.idImage compare:a2.idImage];
        }];
    }
    return self;
}

@end
