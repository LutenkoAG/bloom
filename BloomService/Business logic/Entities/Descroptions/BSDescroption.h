//
//  BSDescroption.h
//  BloomService
//
//  Created by Ruslan Shevtsov on 6/30/16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BSDescroption : NSObject

@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSString *objectId;
@property (nonatomic, strong) NSNumber *partsRate;
@property (nonatomic, strong) NSNumber *laborRate;
@property (nonatomic, strong) NSNumber *repair;
@property (nonatomic, strong) NSString *jCCostCode;
@property (nonatomic, strong) NSNumber *part;
@property (nonatomic, strong) NSString *inactive;
@property (nonatomic, strong) NSString *laborId;

@end
