//
//  BSEquipment.h
//  BloomService
//
//  Created by Aleksandr Lutenko on 9/9/16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BSEquipment : NSObject

@property (nonatomic, strong) NSString *equipmentType;


-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
