//
//  BSEquipment.m
//  BloomService
//
//  Created by Aleksandr Lutenko on 9/9/16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import "BSEquipment.h"
#import "NSDictionary+Extended.h"

@implementation BSEquipment

-(instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (self = [super init]) {
        self.equipmentType = [dictionary tryObjectForKey:@"EquipmentType"];
    }
    return self;
}

@end
