//
//  BSImage.h
//  BloomService
//
//  Created by Aleksandr Lutenko on 29.06.16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

#import "CollectionCell.h"

@interface BSImage : NSObject

@property (strong, nonatomic) NSString *imageUrl;
@property (strong, nonatomic) NSString *bigImageUrl;
@property (strong, nonatomic) NSString *descript;
@property (strong, nonatomic) NSNumber *longitude;
@property (strong, nonatomic) NSNumber *latitude;
@property (strong, nonatomic) NSNumber *idImage;
@property (strong, nonatomic) UIImage *image;

@property (assign, nonatomic) BOOL isUploading;
@property (nonatomic) CGFloat progress;

-(instancetype)initWithDictionary:(NSDictionary *)dictionary;

@end
