//
//  BSImage.m
//  BloomService
//
//  Created by Aleksandr Lutenko on 29.06.16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import "BSImage.h"
#import "NSDictionary+Extended.h"

@implementation BSImage

-(instancetype)initWithDictionary:(NSDictionary *)dictionary {
    if (self = [super init]) {
        NSString *smallimg = [dictionary tryObjectForKey:@"Image"];
//        smallimg = [smallimg stringByReplacingOccurrencesOfString:@"http://devservice.bloomroofing.com/dev/public/userFiles\\images" withString:@"https://bloomservice.blob.core.windows.net/dev/public/userFiles/images/"];
        self.imageUrl = smallimg;
        NSString *bigimg = [dictionary tryObjectForKey:@"BigImage"];
//        bigimg = [bigimg stringByReplacingOccurrencesOfString:@"http://devservice.bloomroofing.com/dev/public/userFiles\\images" withString:@"https://bloomservice.blob.core.windows.net/dev/public/userFiles/images/"];
        self.bigImageUrl = bigimg;
        self.idImage = [dictionary tryObjectForKey:@"Id"];
        self.descript = [dictionary tryObjectForKey:@"Description"];
        self.longitude = [dictionary tryObjectForKey:@"Longitude"];
        self.latitude = [dictionary tryObjectForKey:@"Latitude"];
    }
    return self;
}

@end
