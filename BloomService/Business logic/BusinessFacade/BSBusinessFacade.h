//
//  BSBusinessFacade.h
//  BloomService
//
//  Created by Ruslan Shevtsov on 4/19/16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import <Foundation/Foundation.h>

// DataManager
#import "BSDataManager.h"

@class BSWorkOrder;
@class UIImage;
@class BSPartsOrLabor;
@class BSLocation;
@class BSImage;
@class BSWorkOrderComment;

extern NSString *kGetWorkordersKey;

@interface BSBusinessFacade : NSObject

+ (void)authorizationWithName:(NSString *)name
                     password:(NSString *)password
                   completion:(void(^)(BOOL isSuccess, NSError *error))completion;

+ (void)unauthorization;

+ (BSDataManager *)dataManager;

+ (void)cacheWorkOrder:(BSWorkOrder *)workorder;

+ (BSWorkOrder *)getCachedWorkOrder;

+ (void)updateLocationWithcompletion:(void(^)(BOOL isSuccess, NSError *error))completion;

+ (void)uploadImage:(BSImage *)image
           progress:(void(^)(float progress))progress
         completion:(void(^)(BOOL isSuccess, NSError *error))completion;

+ (void)deleteImage:(BSImage *)image completion:(void(^)(BOOL isSuccess, NSError *error))completion;

+ (void)addNoteImage:(NSNumber *)idImage
         description:(NSString *)description
          completion:(void(^)(BOOL isSuccess, NSError *error))completion;

+ (void)сhangeImageLocation:(NSNumber *)idImage
                   latitude:(NSNumber *)latitude
                  longitude:(NSNumber *)longitude
                 completion:(void(^)(BOOL isSuccess, NSError *error))completion;

+ (void)editEquipment:(BSPartsOrLabor *)equipment
         newEquipment:(BOOL)isNewEquipment
           completion:(void(^)(BOOL isSuccess, NSError *error))completion;

+ (void)deleteEquipment:(BSPartsOrLabor *)equipment
             completion:(void(^)(BOOL isSuccess, NSError *error))completion;

+ (void)updateStatus:(NSString *)status
          completion:(void(^)(BOOL isSuccess, NSError *error))completion;

+ (void)addWorkOrderNotes:(BSWorkOrderComment *)note
               completion:(void(^)(BOOL isSuccess, NSError *error))completion;

+ (void)editWorkOrderNotes:(BSWorkOrderComment *)note
                completion:(void(^)(BOOL isSuccess, NSError *error))completion;

+ (void)deleteWorkOrderNotes:(BSWorkOrderComment *)note
                  completion:(void(^)(BOOL isSuccess, NSError *error))completion;

+ (BSLocation *)currentLocation;

+ (void)updateWorkorders;

+ (BSBusinessFacade *)sharedBusinessFacade;

@end
