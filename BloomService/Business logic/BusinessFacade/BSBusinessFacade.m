//
//  BSBusinessFacade.m
//  BloomService
//
//  Created by Ruslan Shevtsov on 4/19/16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import "BSBusinessFacade.h"

#import <UIKit/UIKit.h>

// Api
#import "ApiService.h"

// Services
#import "BSLocationService.h"

// Entities
#import "BSWorkOrder.h"
#import "BSPartsOrLabor.h"
#import "BSImage.h"
#import "BSWorkOrderComment.h"

static const NSUInteger kUpdateInterval = 30;
static const NSUInteger kLocationUpdateInterval = 60 * 5;

NSString *kGetWorkordersKey = @"getWorkorders";

@interface BSBusinessFacade ()

@property (nonatomic, strong) dispatch_source_t timer;
@property (nonatomic, strong) dispatch_source_t timer2;

@property (nonatomic, strong) BSDataManager *dataMnager;
@property (nonatomic, strong) BSLocationService *locationManager;

@property (nonatomic, strong) BSWorkOrder *currentWorkOrder;

@end

@implementation BSBusinessFacade

+ (void)authorizationWithName:(NSString *)name password:(NSString *)password completion:(void(^)(BOOL isSuccess, NSError *error))completion {
    [ApiService authorizationWithName:name password:password completion:^(BOOL isSuccess, NSString *token, NSError *error) {
        
        if (isSuccess) {
            
            [ApiService loadPartDescriptionsWithToken:token completion:^(BOOL isSuccess, NSArray *descriptions, NSError *error) {
                if (isSuccess) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self sharedBusinessFacade].dataMnager.partDescriptions = descriptions;
                    });
                }
            }];
            
            [ApiService loadRepairDescriptionsWithToken:token completion:^(BOOL isSuccess, NSArray *descriptions, NSError *error) {
                if (isSuccess) {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self sharedBusinessFacade].dataMnager.repairDescriptions = descriptions;
                    });
                }
            }];
        }
        
        dispatch_async(dispatch_get_main_queue(), ^{
            [self sharedBusinessFacade].dataMnager.token = token;
            [[self sharedBusinessFacade] startTimer];
            completion(isSuccess, error);
        });
        
    }];
}

+ (void)unauthorization {
    [[self sharedBusinessFacade] cancelTimers];
}

+ (BSDataManager *)dataManager {
    return [[self sharedBusinessFacade] dataMnager];
}

+ (void)cacheWorkOrder:(BSWorkOrder *)workorder {
    ((BSBusinessFacade *)[self sharedBusinessFacade]).currentWorkOrder = workorder;
}

+ (BSWorkOrder *)getCachedWorkOrder {
    return ((BSBusinessFacade *)[self sharedBusinessFacade]).currentWorkOrder;
}

+ (void)updateLocationWithcompletion:(void(^)(BOOL isSuccess, NSError *error))completion {
    [[self sharedBusinessFacade] sendLocationWithcompletion:completion];
}

+ (void)uploadImage:(BSImage *)bsImage
           progress:(void(^)(float progress))progressBlock
         completion:(void(^)(BOOL isSuccess, NSError *error))completion {
    BSLocation *currentLocation = [[self sharedBusinessFacade].locationManager location];
    BSWorkOrder *currentWorkOrder = [self sharedBusinessFacade].currentWorkOrder;
    bsImage.longitude = @(currentLocation.longitude);
    bsImage.latitude = @(currentLocation.latitude);
    
    [ApiService uploadImage:bsImage.image
                workorderId:currentWorkOrder.workorder
                   location:currentLocation
                      token:[self sharedBusinessFacade].dataMnager.token
                   progress:^(float progress) {
                       progressBlock(progress);
                   }
                 completion:^(BOOL isSuccess, NSError *error) {
                     completion(isSuccess, error);
                 }];
}

+ (void)deleteImage:(BSImage *)image completion:(void(^)(BOOL isSuccess, NSError *error))completion {
    BSWorkOrder *currentWorkOrder = [self sharedBusinessFacade].currentWorkOrder;
    [ApiService deleteImage:image
                workorderId:currentWorkOrder.workorder
                  withToken:[self sharedBusinessFacade].dataMnager.token
                 completion:^(BOOL isSuccess, NSError *error) {
                     if (completion) {
                         dispatch_async(dispatch_get_main_queue(), ^{
                             completion(isSuccess, error);
                         });
                     }
    }];
}

+ (void)addNoteImage:(NSNumber *)idImage description:(NSString *)description completion:(void(^)(BOOL isSuccess, NSError *error))completion {
    BSWorkOrder *currentWorkOrder = [self sharedBusinessFacade].currentWorkOrder;
    [ApiService addNoteWithImageId:idImage
                       workorderId:currentWorkOrder.workorder
                       description:description
                             token:[self sharedBusinessFacade].dataMnager.token
                        completion:^(BOOL isSuccess, NSError *error) {
                            if (completion) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    completion(isSuccess, error);
                                });
                            }
                        }];
}

+ (void)сhangeImageLocation:(NSNumber *)idImage latitude:(NSNumber *)latitude longitude:(NSNumber *)longitude completion:(void(^)(BOOL isSuccess, NSError *error))completion{
    BSWorkOrder *currentWorkOrder = [self sharedBusinessFacade].currentWorkOrder;
    [ApiService changeImageLocation:idImage
                        workorderId:currentWorkOrder.workorder
                          withToken:[self sharedBusinessFacade].dataMnager.token
                           latitude:latitude
                          longitude:longitude
                        completion:^(BOOL isSuccess, NSError *error) {
                            if (completion) {
                                dispatch_async(dispatch_get_main_queue(), ^{
                                    completion(isSuccess, error);
                                });
                            }
                        }];
}

+ (void)editEquipment:(BSPartsOrLabor *)equipment
         newEquipment:(BOOL)isNewEquipment
           completion:(void(^)(BOOL isSuccess, NSError *error))completion {
    [ApiService editEquipment:equipment
                    withToken:[self sharedBusinessFacade].dataMnager.token
                 newEquipment:(BOOL)isNewEquipment
                   completion:^(BOOL isSuccess, BSPartsOrLabor *equipment, NSError *error) {
                       if (completion) {
                           dispatch_async(dispatch_get_main_queue(), ^{
                               if (isSuccess) {
                                   NSPredicate *predicate = [NSPredicate predicateWithFormat:@"equipmentId == %@", equipment.equipmentId];
                                   NSArray *equipments = [[self sharedBusinessFacade].currentWorkOrder.equipments filteredArrayUsingPredicate:predicate];
                                   if ([equipments count]) {
                                       BSPartsOrLabor *oldEquipment = [equipments firstObject];
                                       oldEquipment.name = equipment.name;
                                       oldEquipment.type = equipment.type;
                                       oldEquipment.count = equipment.count;
                                       oldEquipment.rate = equipment.rate;
                                       oldEquipment.repairDescription = equipment.repairDescription;
                                       oldEquipment.date = equipment.date;
                                       oldEquipment.equipmentId = equipment.equipmentId;
                                       oldEquipment.workorderId = equipment.workorderId;
                                       oldEquipment.part = equipment.part;
                                       oldEquipment.employee = equipment.employee;
                                       oldEquipment.costQty = equipment.costQty;
                                   } else {
                                       NSMutableArray *oldEquipments = [NSMutableArray arrayWithArray:[self sharedBusinessFacade].currentWorkOrder.equipments];
                                       [oldEquipments addObject:equipment];
                                       [self sharedBusinessFacade].currentWorkOrder.equipments = [NSArray arrayWithArray:oldEquipments];
                                   }
                                   
                               }
                               completion(isSuccess, error);
                           });
                       }
                   }];
}
+ (void)deleteEquipment:(BSPartsOrLabor *)equipment
           completion:(void(^)(BOOL isSuccess, NSError *error))completion {
    [ApiService deleteEquipment:equipment
                    withToken:[self sharedBusinessFacade].dataMnager.token
                   completion:^(BOOL isSuccess, BSPartsOrLabor *equipment, NSError *error) {
                       if (completion) {
                           dispatch_async(dispatch_get_main_queue(), ^{
                               if (isSuccess) {
                                   NSPredicate *predicate = [NSPredicate predicateWithFormat:@"equipmentId == %@", equipment.equipmentId];
                                   NSArray *equipments = [[self sharedBusinessFacade].currentWorkOrder.equipments filteredArrayUsingPredicate:predicate];
                                   if ([equipments count]) {
                                       BSPartsOrLabor *oldEquipment = [equipments firstObject];
                                       oldEquipment.name = equipment.name;
                                       oldEquipment.type = equipment.type;
                                       oldEquipment.count = equipment.count;
                                       oldEquipment.rate = equipment.rate;
                                       oldEquipment.repairDescription = equipment.repairDescription;
                                       oldEquipment.date = equipment.date;
                                       oldEquipment.equipmentId = equipment.equipmentId;
                                       oldEquipment.workorderId = equipment.workorderId;
                                       oldEquipment.part = equipment.part;
                                       oldEquipment.employee = equipment.employee;
                                       oldEquipment.costQty = equipment.costQty;
                                   } else {
                                       NSMutableArray *oldEquipments = [NSMutableArray arrayWithArray:[self sharedBusinessFacade].currentWorkOrder.equipments];
                                       [oldEquipments addObject:equipment];
                                       [self sharedBusinessFacade].currentWorkOrder.equipments = [NSArray arrayWithArray:oldEquipments];
                                   }
                                   
                               }
                               completion(isSuccess, error);
                           });
                       }
                   }];
}

+ (void)updateStatus:(NSString *)status
          completion:(void(^)(BOOL isSuccess, NSError *error))completion {
    [ApiService updateStatus:status
                 workorderId:[self sharedBusinessFacade].currentWorkOrder.workorder
                   withToken:[self sharedBusinessFacade].dataMnager.token
                  completion:^(BOOL isSuccess, NSError *error) {
                      dispatch_async(dispatch_get_main_queue(), ^{
                          if (isSuccess) {
                              [self sharedBusinessFacade].currentWorkOrder.status = status;
                          }
                          completion(isSuccess, error);
                      });
                  }];
}

+ (void)addWorkOrderNotes:(BSWorkOrderComment *)note
          completion:(void(^)(BOOL isSuccess, NSError *error))completion {
    [ApiService addWorkOrderNotes:note
                      workorderId:[self sharedBusinessFacade].currentWorkOrder.workorder
                        withToken:[self sharedBusinessFacade].dataMnager.token
                          completion:^(BOOL isSuccess, NSError *error) {
                              dispatch_async(dispatch_get_main_queue(), ^{
                          if (isSuccess) {
                              NSMutableArray *comments = [NSMutableArray arrayWithArray:[self sharedBusinessFacade].currentWorkOrder.workOrderComments];
                              [comments addObject: note];
                              [self sharedBusinessFacade].currentWorkOrder.workOrderComments = [NSArray arrayWithArray:comments];
                          }
                          completion(isSuccess, error);
                      });
                  }];
}

+ (void)editWorkOrderNotes:(BSWorkOrderComment *)note
                completion:(void(^)(BOOL isSuccess, NSError *error))completion {
    [ApiService editWorkOrderNotes:note
                       workorderId:[self sharedBusinessFacade].currentWorkOrder.workorder
                         withToken:[self sharedBusinessFacade].dataMnager.token
                        completion:^(BOOL isSuccess, NSError *error) {
                           dispatch_async(dispatch_get_main_queue(), ^{
                               if (isSuccess) {
                                   NSPredicate *predicate = [NSPredicate predicateWithFormat:@"noteId == %@", note.noteId];
                                   NSArray *comments = [[self sharedBusinessFacade].currentWorkOrder.workOrderComments filteredArrayUsingPredicate:predicate];
                                   if ([comments count]) {
                                       BSWorkOrderComment *oldComment = [comments firstObject];
                                       oldComment.notes = note.notes;
                                       oldComment.titel = note.titel;
                                       oldComment.workorderId = [self sharedBusinessFacade].currentWorkOrder.workorder;
                                   }
                               }
                               completion(isSuccess, error);
                           });
                       }];
}

+ (void)deleteWorkOrderNotes:(BSWorkOrderComment *)note
               completion:(void(^)(BOOL isSuccess, NSError *error))completion {
    [ApiService deleteWorkOrderNotes:note
                      workorderId:[self sharedBusinessFacade].currentWorkOrder.workorder
                        withToken:[self sharedBusinessFacade].dataMnager.token
                       completion:^(BOOL isSuccess, NSError *error) {
                           dispatch_async(dispatch_get_main_queue(), ^{
                               completion(isSuccess, error);
                           });
                       }];
}

+ (BSLocation *)currentLocation {
    return [self sharedBusinessFacade].locationManager.location;
}

+ (void)updateWorkorders {
    [[self sharedBusinessFacade] getWorkOrders];
}

+ (BSBusinessFacade *)sharedBusinessFacade {
    static BSBusinessFacade *_sharedBusinessFacade = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedBusinessFacade = [BSBusinessFacade new];
    });
    
    return _sharedBusinessFacade;
}

#pragma mark -
#pragma mark Private

- (instancetype)init {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationDidEnterBackgroundNotification:)
                                                 name:UIApplicationDidEnterBackgroundNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(applicationWillEnterForegroundNotification:)
                                                 name:UIApplicationWillEnterForegroundNotification
                                               object:nil];
    
    _locationManager = [BSLocationService new];
    
    return self;
}

- (void)sendLocationWithcompletion:(void(^)(BOOL isSuccess, NSError *error))completion {
    BSLocation *currentLocation = [self.locationManager location];
    if (currentLocation == nil) {
        return;
    }
    [ApiService updateLocationWithToken:self.dataMnager.token
                               latitude:currentLocation.latitude
                              longitude:currentLocation.longitude
                              completion:completion];
}

-(void)getWorkOrders {
    __weak __typeof(self)weakSelf = self;
    [ApiService workOrderWithtoken:self.dataMnager.token
                        completion:^(BOOL isSuccess, NSArray *workorders, NSError *error) {
                            dispatch_async(dispatch_get_main_queue(), ^{
                                NSArray *sortedLocations = [workorders sortedArrayUsingComparator: ^(BSWorkOrder *a1, BSWorkOrder *a2) {
                                    return [a1.time compare:a2.time];
                                }];
                                weakSelf.dataMnager.workorders = [NSArray arrayWithArray:sortedLocations];
                                [[NSNotificationCenter defaultCenter] postNotificationName:kGetWorkordersKey object:nil];
                            });
                        }];
}

#pragma mark -
#pragma mark Notifications

- (void)applicationDidEnterBackgroundNotification:(NSNotification *)notification {
    __block UIBackgroundTaskIdentifier bgTask = UIBackgroundTaskInvalid;
    
    UIApplication *app = [UIApplication sharedApplication];
    
    bgTask = [app beginBackgroundTaskWithExpirationHandler:^{
        NSLog(@"endBackgroundTask");
        [app endBackgroundTask:bgTask];
        bgTask = UIBackgroundTaskInvalid;
    }];
}

- (void)applicationWillEnterForegroundNotification:(NSNotification *)notification {
    
}

- (void)startServicesInBackground {
    NSLog(@"In background %@", [NSDate date]);
    [self sendLocationWithcompletion:nil];
}

#pragma mark -
#pragma mark Lazy load

- (BSDataManager *)dataMnager {
    if (_dataMnager == nil) {
        _dataMnager = [BSDataManager new];
    }
    return _dataMnager;
}

#pragma mark -
#pragma mark Timer

- (void)startTimer {
    [self getWorkOrders];
    
    dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    double secondsToFire = kUpdateInterval;
    
    __weak __typeof(self)weakSelf = self;
    self.timer = CreateDispatchTimer(secondsToFire, queue, ^{
        UIApplication *app = [UIApplication sharedApplication];
        if (app.applicationState == UIApplicationStateActive) {
            [weakSelf getWorkOrders];
            NSLog(@"Load workorders");
        }
    });
    
    // =============   Update location   =====================
    
    dispatch_queue_t queue2 = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0);
    double secondsToFire2 = kLocationUpdateInterval;
    
    [self sendLocationWithcompletion:nil];
    self.timer2 = CreateDispatchTimer(secondsToFire2, queue2, ^{
        UIApplication *app = [UIApplication sharedApplication];
        if (app.applicationState == UIApplicationStateActive) {
            [weakSelf sendLocationWithcompletion:nil];
        }
    });
}

- (void)cancelTimers {
    if (self.timer) {
        dispatch_source_cancel(self.timer);
        self.timer = nil;
    }
    
    if (self.timer2) {
        dispatch_source_cancel(self.timer2);
        self.timer2 = nil;
    }
}

dispatch_source_t CreateDispatchTimer(double interval, dispatch_queue_t queue, dispatch_block_t block) {
    dispatch_source_t timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, queue);
    if (timer) {
        dispatch_source_set_timer(timer, dispatch_time(DISPATCH_TIME_NOW, interval * NSEC_PER_SEC), interval * NSEC_PER_SEC, (1ull * NSEC_PER_SEC) / 10);
        dispatch_source_set_event_handler(timer, block);
        dispatch_resume(timer);
    }
    return timer;
}

@end
