//
//  ApiService.h
//  BloomService
//
//  Created by Ruslan Shevtsov on 4/19/16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BSLocation;
@class UIImage;
@class BSPartsOrLabor;
@class BSImage;
@class BSWorkOrderComment;

@interface ApiService : NSObject

/**
 * @abstract (POST)
 * @discussion http://service.bloomroofing.com/apimobile/Token?username=asdasd&password=asdasd&grant_type=password
 * @see Documents/ApiIOS_v01.txt
 * @param username and password
 */

+ (void)authorizationWithName:(NSString *)name
                     password:(NSString *)password
                   completion:(void(^)(BOOL isSuccess, NSString *token, NSError *error))completion;


/**
 * @abstract (POST)
 * @discussion http://service.bloomroofing.com/apimobile/location?technicianId=13&lat=45.6679607&lng=46.352323&time=01-01-2016 - send GPS location
 * @see Documents/ApiIOS_v01.txt
 * @param longitude, latitude and token
 */

+ (void)updateLocationWithToken:(NSString *)token
                       latitude:(double)latitude
                      longitude:(double)longitude
                     completion:(void(^)(BOOL isSuccess, NSError *error))completion;

/**
 * @abstract (GET)
 * @discussion http://service.bloomroofing.com/apimobile/workorder
 * @see Documents/ApiIOS_v01.txt
 * @param token
 */

+ (void)workOrderWithtoken:(NSString *)token
                completion:(void(^)(BOOL isSuccess, NSArray *workorders, NSError *error))completion;


/**
 * @abstract (POST)
 * @discussion http://service.bloomroofing.com/apimobile/image - upload image (type variable "file" - string)
 * @see Documents/ApiIOS_v01.txt
 * @param workorderId, location, image
 */

+ (void)uploadImage:(UIImage *)image
        workorderId:(NSString *)workorderId
           location:(BSLocation *)lacation
              token:(NSString *)token
           progress:(void(^)(float progress))progress
         completion:(void(^)(BOOL isSuccess, NSError *error))completion;

/**
 * @abstract (POST)
 * @discussion http://service.bloomroofing.com/apimobile/DeletePicture - add notes
 * @param BSImage, workorderId, token
 */

+ (void)deleteImage:(BSImage *)image
        workorderId:(NSString *)workorderId
          withToken:(NSString *)token
         completion:(void(^)(BOOL isSuccess, NSError *error))completion;


/**
 * @abstract (POST)
 * @discussion http://service.bloomroofing.com/apimobile/CommentImage - add notes
 * @see Documents/ApiIOS_v01.txt
 * @param workorderId, IdImage, description
*/

+ (void)addNoteWithImageId:(NSNumber *)idImage
               workorderId:(NSString *)workorderId
               description:(NSString *)description
                     token:(NSString *)token
                completion:(void(^)(BOOL isSuccess, NSError *error))completion;

/**
 * @abstract (GET)
 * @discussion http://service.bloomroofing.com/apimobile/Part - load part descriptions
 * @see Documents/ApiIOS_v01.txt
 */

+ (void)loadPartDescriptionsWithToken:(NSString *)token
                           completion:(void(^)(BOOL isSuccess, NSArray *descriptions, NSError *error))completion;

/**
 * @abstract (GET)
 * @discussion http://service.bloomroofing.com/apimobile/Repair - load repair descriptions
 * @see Documents/ApiIOS_v01.txt
 */

+ (void)loadRepairDescriptionsWithToken:(NSString *)token
                             completion:(void(^)(BOOL isSuccess, NSArray *descriptions, NSError *error))completion;


/**
 * @abstract (POST)
 * @discussion (POST) http://service.bloomroofing.com/apimobile/AddItem - add or edit equipment
 * @see Documents/ApiIOS_v01.txt
 */

+ (void)editEquipment:(BSPartsOrLabor *)equipment
            withToken:(NSString *)token
         newEquipment:(BOOL)isNewEquipment
           completion:(void(^)(BOOL isSuccess, BSPartsOrLabor *equipment, NSError *error))completion;


/**
 * @abstract (POST)
 * @discussion (POST) http://service.bloomroofing.com/apimobile/DeleteItem - add or edit equipment
 * @see Documents/ApiIOS_v01.txt
 */

+ (void)deleteEquipment:(BSPartsOrLabor *)equipment
            withToken:(NSString *)token
           completion:(void(^)(BOOL isSuccess, BSPartsOrLabor *equipment, NSError *error))completion;

/**
 * @abstract (POST)
 * @discussion (POST) http://service.bloomroofing.com/apimobile/ChangeWorkorderStatus - update status
 * @see Documents/ApiIOS_v01.txt
 */

+ (void)updateStatus:(NSString *)status
         workorderId:(NSString *)workorderId
           withToken:(NSString *)token
          completion:(void(^)(BOOL isSuccess, NSError *error))completion;

/**
 * @abstract (POST)
 * @discussion (POST) http://service.bloomroofing.com/apimobile/AddNote - add WorkOrder Notes
 * @see Documents/ApiIOS_v01.txt
 */

+ (void)addWorkOrderNotes:(BSWorkOrderComment *)note
              workorderId:(NSString *)workorderId
                withToken:(NSString *)token
               completion:(void(^)(BOOL isSuccess, NSError *error))completion;

/**
 * @abstract (POST)
 * @discussion (POST) http://service.bloomroofing.com/apimobile/EditNote - edit WorkOrder Notes
 * @see Documents/ApiIOS_v01.txt
 */

+ (void)editWorkOrderNotes:(BSWorkOrderComment *)note
               workorderId:(NSString *)workorderId
                 withToken:(NSString *)token
                completion:(void(^)(BOOL isSuccess, NSError *error))completion;

/**
 * @abstract (POST)
 * @discussion (POST) http://service.bloomroofing.com/apimobile/DeleteNote - edit WorkOrder Notes
 * @see Documents/ApiIOS_v01.txt
 */

+ (void)deleteWorkOrderNotes:(BSWorkOrderComment *)note
               workorderId:(NSString *)workorderId
                 withToken:(NSString *)token
                completion:(void(^)(BOOL isSuccess, NSError *error))completion;

/**
 * @abstract (POST)
 * @discussion (POST) http://service.bloomroofing.com/apimobile/ChangeImageLocation - add WorkOrder Notes
 * @see Documents/ApiIOS_v01.txt
 */

+ (void)changeImageLocation:(NSNumber *)idImage
                workorderId:(NSString *)workorderId
                  withToken:(NSString *)token
                   latitude:(NSNumber *)latitude
                  longitude:(NSNumber *)longitude
                 completion:(void(^)(BOOL isSuccess, NSError *error))completion;



@end
