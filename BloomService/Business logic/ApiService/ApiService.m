//
//  ApiService.m
//  BloomService
//
//  Created by Ruslan Shevtsov on 4/19/16.
//  Copyright © 2016 iQueSoft. All rights reserved.
//

#import "ApiService.h"

// Entities
#import "BSWorkOrder.h"
#import "NSDictionary+Extended.h"
#import "BSRepairDescription.h"
#import "BSPartDescription.h"
#import "NSDictionary+Extended.h"
#import <UIKit/UIKit.h>
#import "BSLocationService.h"
#import "BSPartsOrLabor.h"
#import "AppDelegate.h"
#import "BSImage.h"
#import "BSWorkOrderComment.h"

// Frameworks
#import "AFNetworking.h"

static NSString * const kBasePathProduction = @"http://service.bloomroofing.com/apimobile";
static NSString * const kBasePathDev = @"http://devservice.bloomroofing.com/apimobile";
static NSString * const kBasePathBeta = @"http://betaservice.bloomroofing.com/apimobile";

static NSString * const kAuthorization = @"authorization";
static NSString * const kLocation = @"location";
static NSString * const kWorkorder = @"workorder";
static NSString * const kImage = @"image";
static NSString * const kDeviceToken = @"devicetoken";
static NSString * const kCommentImage = @"CommentImage";
static NSString * const kPart = @"Part";
static NSString * const kRepair = @"Repair";
static NSString * const kAddItem = @"AddItem";
static NSString * const kUpdateStus = @"ChangeWorkorderStatus";
static NSString * const kDeleteItem = @"DeleteItem";
static NSString * const kDeletePicture = @"DeletePicture";
static NSString * const kChangeWorkOrderNotes = @"ChangeWorkOrderNotes";
static NSString * const kEditNote = @"EditNote";
static NSString * const kAddNote = @"AddNote";
static NSString * const kDeleteNote = @"DeleteNote";
static NSString * const kChangeImageLocation = @"ChangeImageLocation";

@implementation ApiService

+ (NSString *)basePath {
#ifdef BETA
    return kBasePathBeta;
#else
    return kBasePathDev;
#endif
}

+ (void)authorizationWithName:(NSString *)name
                     password:(NSString *)password
                   completion:(void(^)(BOOL isSuccess, NSString *token, NSError *error))completion {
//    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
//    NSString *str = appDelegate.deviceTokenString;
//    if (str == nil) {
//        completion (NO, nil, nil);
//        return;
//    }
    NSDictionary *parameters = @{@"name": name, @"password": password};//, @"deviceToken": str};
    [self postRequest:kAuthorization
           parameters:parameters
                 body:nil
             headers:@{@"Content-Type": @"application/json"}
          completion:^(BOOL isSuccess, NSData *data, NSURLResponse *response, NSError *error) {
              if (error) {
                  completion(NO, nil, nil);
              } else {
                  id json = [NSJSONSerialization JSONObjectWithData:data
                                                            options:0
                                                              error:nil];
                  
                  NSString *token = json[@"access_token"];
                  if (token == nil){
                      completion (NO, nil, nil);
                  } else {
                      completion(YES, token, nil);
                  }
              }
          }];
}

+ (void)workOrderWithtoken:(NSString *)token
                   completion:(void(^)(BOOL isSuccess, NSArray *workorders, NSError *error))completion {
    [self getRequest:kWorkorder
          parameters:nil
             headers:@{@"Authorization": [NSString stringWithFormat:@"bearer %@", token] ,@"Content-Type": @"application/json"}
          completion:^(BOOL isSuccess, NSData *data, NSURLResponse *response, NSError *error) {
              if (error) {
                  completion(NO, nil, nil);
              } else {
                  id json = [NSJSONSerialization JSONObjectWithData:data
                                                            options:0
                                                              error:nil];
                  
                  NSMutableArray *workorders = [NSMutableArray array];
                  
                  if ([json isKindOfClass:[NSArray class]]) {
                      [json enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                          if ([obj isKindOfClass:[NSDictionary class]]) {
                              BSWorkOrder *workOrder = [[BSWorkOrder alloc] initWithDictionary: obj];
                              
                              [workorders addObject:workOrder];
                          }
                      }];
                  }
                  
                  completion(YES, workorders, nil);
              }
          }];
}

+ (void)uploadImage:(UIImage *)image
        workorderId:(NSString *)workorderId
           location:(BSLocation *)lacation
              token:(NSString *)token
           progress:(void(^)(float progress))progress
         completion:(void(^)(BOOL isSuccess, NSError *error))completion {
    
    
    NSDictionary *parameters = @{@"Latitude": @(lacation.latitude),
                                 @"Longitude": @(lacation.longitude),
                                 @"IdWorkOrder": @([workorderId integerValue])};
    
    NSString *urlString = [NSString stringWithFormat:@"%@/%@", [self basePath], kImage];
    
    NSDictionary *headers = @{@"Authorization": [NSString stringWithFormat:@"bearer %@", token], @"Content-Type": @"application/json"};
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST"
                                                                                              URLString:urlString
                                                                                             parameters:parameters
                                                                              constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
                                                                                  [formData appendPartWithFileData:UIImagePNGRepresentation(image)
                                                                                                              name:@"Image"
                                                                                                          fileName:@"photo.png"
                                                                                                          mimeType:@"image/png"];
                                                                              } error:nil];
    
    [headers enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        [request addValue:obj forHTTPHeaderField:key];
    }];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      
                      float p = (float)uploadProgress.completedUnitCount / (float)uploadProgress.totalUnitCount;
                      NSLog(@"Image uploading progress - %f", p);
                      progress(p);
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      NSLog(@"\nResponse URL: %@\nResponse Object: %@", urlString, responseObject);
                      NSError *customError;
                      if (error) {
                          customError = error;
                      } else {
                          customError = [self createCustomErrorIfNeededWithJson:responseObject];
                      }
                      completion((customError == nil), customError);
                  }];
    
    [uploadTask resume];
}

+ (void)deleteImage:(BSImage *)image
        workorderId:(NSString *)workorderId
          withToken:(NSString *)token
         completion:(void(^)(BOOL isSuccess, NSError *error))completion {
    
    NSDictionary * body = @{@"WorkOrder" : workorderId,
                                  @"Id" : image.idImage};
    
    [self postRequest:kDeletePicture
           parameters:nil
                 body:body
              headers:@{@"Authorization": [NSString stringWithFormat:@"bearer %@", token]}
           completion:^(BOOL isSuccess, NSData *data, NSURLResponse *response, NSError *error) {
               if (completion) {
                   completion(isSuccess, error);
               }
    }];
}

+ (void)changeImageLocation:(NSNumber *)idImage
                workorderId:(NSString *)workorderId
                  withToken:(NSString *)token
                   latitude:(NSNumber *)latitude   
                  longitude:(NSNumber *)longitude

                completion:(void(^)(BOOL isSuccess, NSError *error))completion {
    
    NSDictionary *body = @{@"WorkOrderId": workorderId,
                           @"PictureId": idImage != nil ? idImage : @(0),
                           @"Latitude": latitude != nil ? latitude : @"",
                           @"Longitude": longitude != nil ? longitude : @""};
    
    [self postRequest:kChangeImageLocation
           parameters:nil
                 body:body
              headers:@{@"Authorization": [NSString stringWithFormat:@"bearer %@", token]}
           completion:^(BOOL isSuccess, NSData *data, NSURLResponse *response, NSError *error) {
               completion(isSuccess, error);
           }];
}

+ (void)updateLocationWithToken:(NSString *)token
                       latitude:(double)latitude
                      longitude:(double)longitude
                     completion:(void(^)(BOOL isSuccess, NSError *error))completion {
    
    NSDictionary *parameters = @{@"lat": @(latitude),
                                 @"lng": @(longitude)};
    
    NSDictionary *headers = @{@"Authorization": [NSString stringWithFormat:@"bearer %@", token]};
    
    [self postRequest:kLocation
           parameters:parameters
                 body:nil
              headers:headers
           completion:^(BOOL isSuccess, NSData *data, NSURLResponse *response, NSError *error) {
               if (completion) {
                   completion(isSuccess, error);
               }
           }];
}

+ (void)addNoteWithImageId:(NSNumber *)idImage
               workorderId:(NSString *)workorderId
               description:(NSString *)description
                     token:(NSString *)token
                completion:(void(^)(BOOL isSuccess, NSError *error))completion {

    NSDictionary *body = @{@"IdImage": idImage,
                           @"IdWorkOrder": @([workorderId integerValue]),
                           @"Description": description};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    NSString *urlString = [NSString stringWithFormat:@"%@/%@", [self basePath], kCommentImage];
    
    AFJSONRequestSerializer *serializer = [AFJSONRequestSerializer serializer];
    [serializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [serializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [serializer setValue:[NSString stringWithFormat:@"bearer %@", token] forHTTPHeaderField:@"Authorization"];
    manager.requestSerializer = serializer;
    
    NSLog(@"\nPOST\n URL: %@/%@\nbody: %@", [self basePath], urlString, body);
    
    [manager POST:urlString
       parameters:body
         progress:nil
          success:^(NSURLSessionDataTask *task, id responseObject) {
              NSLog(@"\nResponse URL: %@/%@\nResponse Object: %@", [self basePath], urlString, responseObject);
              NSError *customError = [self createCustomErrorIfNeededWithJson:responseObject];
              completion((customError == nil), customError);
          } failure:^(NSURLSessionDataTask *task, NSError *error) {
              NSLog(@"\nURL: %@/%@\n Error - %@", [self basePath], urlString, error.localizedDescription);
              completion(NO, error);
          }];
}

+ (void)loadPartDescriptionsWithToken:(NSString *)token
                           completion:(void(^)(BOOL isSuccess, NSArray *descriptions, NSError *error))completion {
    [self getRequest:kPart
          parameters:nil
             headers:@{@"Authorization": [NSString stringWithFormat:@"bearer %@", token]}
          completion:^(BOOL isSuccess, NSData *data, NSURLResponse *response, NSError *error) {
              
              id json = [NSJSONSerialization JSONObjectWithData:data
                                                        options:0
                                                          error:nil];
              
              NSMutableArray *partDescriptions = [NSMutableArray array];
              
              [json enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                  BSPartDescription *partDescription = [BSPartDescription new];
                  partDescription.objectId = [obj tryObjectForKey:@"Id"];
                  partDescription.text = [obj tryObjectForKey:@"Description"];
                  partDescription.partsRate = [obj tryObjectForKey:@"Level1Price"];
                  partDescription.part = [obj tryObjectForKey:@"Part"];
                  [partDescriptions addObject:partDescription];
              }];
              
              if (completion) {
                  completion(isSuccess, partDescriptions, error);
              }
          }];
}

+ (void)loadRepairDescriptionsWithToken:(NSString *)token
                             completion:(void(^)(BOOL isSuccess, NSArray *descriptions, NSError *error))completion {
    [self getRequest:kRepair
          parameters:nil
             headers:@{@"Authorization": [NSString stringWithFormat:@"bearer %@", token]}
          completion:^(BOOL isSuccess, NSData *data, NSURLResponse *response, NSError *error) {
              
              id json = [NSJSONSerialization JSONObjectWithData:data
                                                        options:0
                                                          error:nil];
              
              NSMutableArray *repairDescriptions = [NSMutableArray array];
              
              [json enumerateObjectsUsingBlock:^(id  _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
                  BSRepairDescription *repairDescription = [BSRepairDescription new];
                  repairDescription.objectId = [obj tryObjectForKey:@"Id"];
                  repairDescription.text = [obj tryObjectForKey:@"Description"];
                  repairDescription.laborRate = @(85);
                  repairDescription.jCCostCode = [obj tryObjectForKey:@"JCCostCode"];
                  repairDescription.repair = [obj tryObjectForKey:@"Repair"];
                  repairDescription.inactive = [obj tryObjectForKey:@"Inactive"];
                  [repairDescriptions addObject:repairDescription];
              }];
              
              if (completion) {
                  completion(isSuccess, repairDescriptions, error);
              }
          }];
}

+ (void)editEquipment:(BSPartsOrLabor *)equipment
            withToken:(NSString *)token
         newEquipment:(BOOL)isNewEquipment
           completion:(void(^)(BOOL isSuccess, BSPartsOrLabor *equipment, NSError *error))completion {
    if (!equipment) {
        completion(NO, nil, nil);
        return;
    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd"];
    NSString *stringFromDate = [formatter stringFromDate:equipment.date];
    
    NSMutableDictionary *laborItem = [NSMutableDictionary new];
    BSRepairDescription *repairDescription = equipment.repairDescription;
    
    if (repairDescription != nil) {
        [laborItem trySetObject: repairDescription.text forKey:@"Description"];
        [laborItem trySetObject: repairDescription.objectId forKey:@"Id"];
        [laborItem trySetObject: repairDescription.inactive forKey:@"Inactive"];
        [laborItem trySetObject: repairDescription.jCCostCode forKey:@"JCCostCode"];
        [laborItem trySetObject: repairDescription.repair forKey:@"Repair"];
    }
    
    NSDictionary *body = @{@"WorkOrder": equipment.workorderId,
                           @"WorkOrderItem": equipment.equipmentId,
                           @"Type": equipment.type,
                           @"BiledQty": equipment.count,
                           @"WorkDate": stringFromDate,
                           @"Description": equipment.name,
                           @"LaborItem": laborItem,
                           @"Rate": equipment.rate,
                           @"Part": equipment.part != nil ? equipment.part : @"",
                           @"CostQty": equipment.costQty != nil ? equipment.costQty : @""};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    AFJSONRequestSerializer *serializer = [AFJSONRequestSerializer serializer];
    [serializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [serializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [serializer setValue:[NSString stringWithFormat:@"bearer %@", token] forHTTPHeaderField:@"Authorization"];
    manager.requestSerializer = serializer;
    
    NSString *urlString = [NSString stringWithFormat:@"%@/%@", [self basePath], kAddItem];

    NSLog(@"\nPOST\n URL: %@/%@\nbody: %@", [self basePath], urlString, body);
    
    [manager POST:urlString
       parameters:body
         progress:nil
          success:^(NSURLSessionDataTask *task, id responseObject) {
              
              NSLog(@"\nResponse URL: %@/%@\nResponse Object: %@", [self basePath], urlString, responseObject);
              NSError *customError = [self createCustomErrorIfNeededWithJson:responseObject];
              
              if (customError != nil) {
                  completion(NO, nil, customError);
                  return;
              }

              BSPartsOrLabor *equipmentFromServer;
              
              if (isNewEquipment) {
                  equipmentFromServer = [BSPartsOrLabor new];
              } else {
                  equipmentFromServer = equipment;
              }
              
              NSDictionary *equipmentDict = [responseObject tryObjectForKey:@"Entity"];
              equipmentFromServer.name = [equipmentDict tryObjectForKey:@"Description"];
              equipmentFromServer.count = [equipmentDict tryObjectForKey:@"Quantity"];
              equipmentFromServer.type = [equipmentDict tryObjectForKey:@"ItemType"];
              equipmentFromServer.rate = [equipmentDict tryObjectForKey:@"UnitSale"];
              equipmentFromServer.repairDescription = [equipmentDict tryObjectForKey:@"LaborItem"];
              NSString *date = [equipmentDict tryObjectForKey:@"WorkDate"];
              date = [date stringByReplacingOccurrencesOfString:@"T" withString:@" "];
              date = [date stringByReplacingOccurrencesOfString:@"Z" withString:@""];
              NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
              [formatter1 setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
              equipmentFromServer.date = [formatter1 dateFromString:date];
              equipmentFromServer.equipmentId = [equipmentDict tryObjectForKey:@"WorkOrderItem"];
              equipmentFromServer.workorderId = [equipmentDict tryObjectForKey:@"WorkOrder"];
              equipmentFromServer.part = [equipmentDict tryObjectForKey:@"Part"];
              equipmentFromServer.costQty = [equipmentDict tryObjectForKey:@"CostQuantity"];
              equipmentFromServer.employee = [equipmentDict tryObjectForKey:@"Employee"];
              equipmentFromServer.amount = [equipmentDict tryObjectForKey:@"TotalSale"];
              
              completion(YES, equipmentFromServer, nil);
     
     } failure:^(NSURLSessionDataTask *task, NSError *error) {
         NSLog(@"\nURL: %@/%@\n Error - %@", [self basePath], urlString, error.localizedDescription);
              completion(NO, nil, error);
          }];
}

+ (void)deleteEquipment:(BSPartsOrLabor *)equipment
            withToken:(NSString *)token
           completion:(void(^)(BOOL isSuccess, BSPartsOrLabor *equipment, NSError *error))completion {
    if (!equipment) {
        completion(NO, nil, nil);
        return;
    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd"];
    NSString *stringFromDate = [formatter stringFromDate:equipment.date];
    
    NSMutableDictionary *laborItem = [NSMutableDictionary new];
    BSRepairDescription *repairDescription = equipment.repairDescription;
    
    if (repairDescription != nil) {
        [laborItem trySetObject: repairDescription.text forKey:@"Description"];
        [laborItem trySetObject: repairDescription.objectId forKey:@"Id"];
        [laborItem trySetObject: repairDescription.inactive forKey:@"Inactive"];
        [laborItem trySetObject: repairDescription.jCCostCode forKey:@"JCCostCode"];
        [laborItem trySetObject: repairDescription.repair forKey:@"Repair"];
    }
    
    NSDictionary *body = @{@"WorkOrder": equipment.workorderId,
                           @"WorkOrderItem": equipment.equipmentId,
                           @"Type": equipment.type,
                           @"BiledQty": equipment.count,
                           @"WorkDate": stringFromDate,
                           @"Description": equipment.name,
                           @"LaborItem": laborItem,
                           @"Rate": equipment.rate,
                           @"Part": equipment.part != nil ? equipment.part : @"",
                           @"CostQty": equipment.costQty != nil ? equipment.costQty : @""};
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    AFJSONRequestSerializer *serializer = [AFJSONRequestSerializer serializer];
    [serializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [serializer setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [serializer setValue:[NSString stringWithFormat:@"bearer %@", token] forHTTPHeaderField:@"Authorization"];
    manager.requestSerializer = serializer;
    
    NSString *urlString = [NSString stringWithFormat:@"%@/%@", [self basePath], kDeleteItem];
    
    NSLog(@"\nPOST\n URL: %@/%@\nbody: %@", [self basePath], urlString, body);
    
    [manager POST:urlString
       parameters:body
         progress:nil
          success:^(NSURLSessionDataTask *task, id responseObject) {
              NSLog(@"\nResponse URL: %@/%@\nResponse Object: %@", [self basePath], urlString, responseObject);
              
              NSError *customError = [self createCustomErrorIfNeededWithJson:responseObject];
              completion((customError == nil), equipment, customError);
              
          } failure:^(NSURLSessionDataTask *task, NSError *error) {
              
              completion(NO, nil, error);
          }];
}

+ (void)updateStatus:(NSString *)status
         workorderId:(NSString *)workorderId
           withToken:(NSString *)token
          completion:(void(^)(BOOL isSuccess, NSError *error))completion {
    
    NSDictionary *body = @{@"Id": workorderId,
                           @"Status": status};
    
    [self postRequest:kUpdateStus
           parameters:nil
                 body:body
              headers:@{@"Authorization": [NSString stringWithFormat:@"bearer %@", token]}
           completion:^(BOOL isSuccess, NSData *data, NSURLResponse *response, NSError *error) {
               completion(isSuccess, error);
           }];
}
+ (void)addWorkOrderNotes:(BSWorkOrderComment *)note
              workorderId:(NSString *)workorderId
                withToken:(NSString *)token
               completion:(void(^)(BOOL isSuccess, NSError *error))completion {
    
    NSDictionary *body = @{@"WorkOrderId": workorderId,
                           @"NoteId": note.noteId != nil ? note.noteId : @(0),
                           @"SubjectLine": note.titel != nil ? note.titel : @"",
                           @"Text": note.notes != nil ? note.notes : @""};
    
    [self postRequest:kAddNote
           parameters:nil
                 body:body
              headers:@{@"Authorization": [NSString stringWithFormat:@"bearer %@", token]}
           completion:^(BOOL isSuccess, NSData *data, NSURLResponse *response, NSError *error) {
               completion(isSuccess, error);
           }];
}

+ (void)editWorkOrderNotes:(BSWorkOrderComment *)note
              workorderId:(NSString *)workorderId
                withToken:(NSString *)token
               completion:(void(^)(BOOL isSuccess, NSError *error))completion {
    
    NSDictionary *body = @{@"WorkOrderId": workorderId,
                           @"NoteId": note.noteId != nil ? note.noteId : @(0),
                           @"SubjectLine": note.titel != nil ? note.titel : @"",
                           @"Text": note.notes != nil ? note.notes : @""};
    
    [self postRequest:kEditNote
           parameters:nil
                 body:body
              headers:@{@"Authorization": [NSString stringWithFormat:@"bearer %@", token]}
           completion:^(BOOL isSuccess, NSData *data, NSURLResponse *response, NSError *error) {
               completion(isSuccess, error);
           }];
}

+ (void)deleteWorkOrderNotes:(BSWorkOrderComment *)note
                 workorderId:(NSString *)workorderId
                   withToken:(NSString *)token
                  completion:(void(^)(BOOL isSuccess, NSError *error))completion {

    NSDictionary *body = @{@"WorkOrderId": workorderId,
                           @"NoteId": note.noteId != nil ? note.noteId : @(0),
                           @"SubjectLine": note.titel != nil ? note.titel : @"",
                           @"Text": note.notes != nil ? note.notes : @""};

    [self postRequest:kDeleteNote
           parameters:nil
                 body:body
              headers:@{@"Authorization": [NSString stringWithFormat:@"bearer %@", token]}
           completion:^(BOOL isSuccess, NSData *data, NSURLResponse *response, NSError *error) {
               completion(isSuccess, error);
           }];
}

#pragma mark -
#pragma mark Private

+ (NSURL *)url:(NSString *)subPath {
    return [self url:subPath parameters:nil];
}

+ (NSURL *)url:(NSString *)subPath parameters:(NSDictionary *)parameters {
    if ([[parameters allKeys] count]) {
        NSMutableString *parametersString = [[NSMutableString alloc] init];
        
        [parameters enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            [parametersString appendFormat:@"%@=%@&", key, obj];
        }];
        
        return [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@?%@", [self basePath], subPath, parametersString]];
    }
    return [NSURL URLWithString:[NSString stringWithFormat:@"%@/%@", [self basePath], subPath]];
}

// !!!: Для отправки GET запросов. В случае если в документации (GET) http://service...

+ (void)getRequest:(NSString *)urlString
        parameters:(NSDictionary *)parameters
           headers:(NSDictionary *)headers
        completion:(void(^)(BOOL isSuccess, NSData *data, NSURLResponse *response, NSError *error))completion {
    
    NSLog(@"\nGET\n URL: %@/%@\nParameters: %@", [self basePath], urlString, parameters);
    
    NSURL *url = [self url:urlString parameters:parameters];
    
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    request.HTTPMethod = @"GET";
    
    if ([[headers allKeys] count]) {
        [headers enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
            [request addValue:obj forHTTPHeaderField:key];
        }];
    }

    NSURLSessionDataTask *task = [[NSURLSession sharedSession] dataTaskWithRequest:request
                                                                 completionHandler:^(NSData *data,
                                                                                     NSURLResponse *response,
                                                                                     NSError *error) {
                                                                     NSError *customError;
                                                                     if (error) {
                                                                         customError = error;
                                                                     } else {
                                                                         customError = [self handleResponseWithData:data urlString:urlString];
                                                                     }
                                                                     completion((customError == nil), data, response, customError);
     }];
    [task resume];
}

// !!!: Для отправки POST запросов. В случае если в документации (POST) http://service...

+ (void)postRequest:(NSString *)urlString
         parameters:(NSDictionary *)parameters
               body:(NSDictionary *)body
            headers:(NSDictionary *)headers
         completion:(void(^)(BOOL isSuccess, NSData *data, NSURLResponse *response, NSError *error))completion {
    NSURLSession *defaultSession = [NSURLSession sessionWithConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]
                                                                 delegate:nil
                                                            delegateQueue:[NSOperationQueue mainQueue]];
    NSLog(@"\nPOST\n URL: %@/%@\nParameters: %@\nbody: %@", [self basePath], urlString, parameters, body);
    
    NSURL *url = [self url:urlString parameters:parameters];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    
    if ([[body allKeys] count]) {
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:body
                                                           options:NSJSONWritingPrettyPrinted
                                                             error:nil];
        request.HTTPBody = jsonData;
    }
    
    if ([[headers allKeys] count]) {
        [headers enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
            [request addValue:obj forHTTPHeaderField:key];
        }];
    }
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSURLSessionDataTask * dataTask = [defaultSession dataTaskWithRequest:request
                                                        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                            NSError *customError;
                                                            if (error) {
                                                                customError = error;
                                                            } else {
                                                                customError = [self handleResponseWithData:data urlString:urlString];
                                                            }
                                                            completion((customError == nil), data, response, customError);
    }];
    [dataTask resume];
}

+ (NSError *)handleResponseWithData:(NSData *)data urlString:(NSString *)urlString {
    if (data == nil) {
        NSLog(@"\nURL: %@/%@\n Received data = nil", [self basePath], urlString);
        return [self createCustomErrorIfNeededWithJson:@{@"success" : @(0), @"message" : @"Received data is nil", @"innerError" : @"Received data is nil"}];
    } else {
        id json = [NSJSONSerialization JSONObjectWithData:data
                                                  options:0
                                                    error:nil];
        NSLog(@"\nResponse URL: %@/%@\nParameters: %@", [self basePath], urlString, json);
        return [self createCustomErrorIfNeededWithJson:json];
    }
}

+ (NSError *)createCustomErrorIfNeededWithJson:(id)json {
    if (![json isKindOfClass:[NSArray class]]) {
        if ([json tryObjectForKey:@"success"] != nil &&
            [[json tryObjectForKey:@"success"] integerValue] == 0) {
            NSInteger code = [[json tryObjectForKey:@"code"] integerValue];
            NSError *customError = [NSError errorWithDomain: @"BloomServiceApi" code: code userInfo:@{NSLocalizedDescriptionKey : [json tryObjectForKey:@"message"], @"innerError" : [json tryObjectForKey:@"innerError"]}];
            return customError;
        }
        return nil;
    }
    return nil;
}

#pragma mark -
#pragma mark Helpers

+ (NSString *)encodeToBase64String:(UIImage *)image {
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
}

@end
